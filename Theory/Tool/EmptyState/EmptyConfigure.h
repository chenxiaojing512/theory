//
//  EmptyConfigure.h
//  Join
//
//  Created by JOIN iOS on 2018/8/17.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmptyConfigure : NSObject

/**背景颜色 默认0xf2f2f2*/
@property (strong, nonatomic) UIColor *color;
/**整体centeryY 偏移 默认-44*/
@property (assign, nonatomic) double offsetY;


#pragma mark - 图片
/**图片*/
@property (strong, nonatomic) UIImage *image;
/**图片昵称 (本地图片)*/
@property (copy, nonatomic) NSString *img_name;
/**图片URL (网络图片)*/
@property (copy, nonatomic) NSString *img_url;
/*该属性只有网络图片有效，反之无效 默认60*60*/
@property (nonatomic) CGSize img_size;


#pragma mark - 提示语
/**内容*/
@property (copy, nonatomic) NSString *lb_title;
/**字体 默认system15*/
@property (strong, nonatomic) UIFont *lb_font;
/**文字颜色 默认0x545454*/
@property (strong, nonatomic) UIColor *lb_color;
/**提示语顶部-图片底部 间隔 默认32.0*/
@property (assign, nonatomic) double content_top;


#pragma mark - 按钮
/**按钮文案 当该属性为nil 则不显示按钮*/
@property (copy, nonatomic) NSString *bt_title;
/**按钮文字字体 默认system15*/
@property (strong, nonatomic) UIFont *bt_font;
/**按钮文字颜色 0x111111*/
@property (strong, nonatomic) UIColor *bt_color;
/**按钮背景颜色 0xffd700*/
@property (strong, nonatomic) UIColor *bt_bgColor;
/**按钮圆角 默认2.0*/
@property (assign, nonatomic) double bt_radius;
/**按钮顶部-提示语底部(提示语存在)、按钮顶部-图片底部(提示语不存在)  间隔 默认36.0*/
@property (assign, nonatomic) double bt_top;
/**按钮size 默认 126.0、36.0*/
@property (nonatomic) CGSize bt_size;



#pragma mark - 标识
/**自定义标识，用于项目特殊判断(和空状态UI无关)*/
@property (assign, nonatomic) NSInteger kjTag;

@end
