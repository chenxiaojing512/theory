//
//  EmptyStateView.m
//  Join
//
//  Created by JOIN iOS on 2018/1/6.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import "EmptyStateView.h"
#import <Masonry.h>

@interface EmptyStateView ()

@property (copy, nonatomic) TapButtonAction tapBtnBlock;
@property (copy, nonatomic) TapBackgroundAction tapBgdBlock;
@property (assign, nonatomic) int typeIndex;

@end

@implementation EmptyStateView


/**清除空状态页面*/
+ (void)clearFromSuperView:(UIView *)sView {
    for (UIView *view in sView.subviews) {
        if ([view isKindOfClass:[EmptyStateView class]]) {
            [view removeFromSuperview];
            break;
        }
    }
}

/**
 空状态展示 v2
 
 @param config 配置
 @param sView superV
 @param btnAction 按钮点击的回调
 @param action 背景点击的回调
 */
+ (void)kjEmptyConfig:(EmptyConfigure *)config
                sView:(UIView *)sView
         buttonAction:(TapButtonAction)btnAction
     backgroundAction:(TapBackgroundAction)action {
    //查询sView上是否已经有该view了
    EmptyStateView *eView;
    BOOL isExist = NO;//记录在sView是否已经存在eView，避免重复addSubview
    for (UIView *view in sView.subviews) {
        if ([view isKindOfClass:[EmptyStateView class]]) {
            eView = (EmptyStateView *)view;
            isExist = YES;
            break;
        }
    }
    if (!config) {//如果type为负数，则删除显示
        if (eView) {
            [eView removeFromSuperview];
            eView = nil;
        }
        return;
    }
    
    if (eView) {
        //如果已经存在但是状态改变了，则重新创建子视图
        [eView removeAllSubviews];
    } else {
        //如果不存在  则创建
        eView = [[EmptyStateView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(sView.frame), CGRectGetHeight(sView.frame))];
//        [eView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(sView);
//        }];
    }
    
    eView.backgroundColor = config.color;
    eView.tapBtnBlock = btnAction;//点击按钮的回调
    eView.tapBgdBlock = action;//点击背景的回调
    
    //子视图的背景view
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:eView action:@selector(didTapBackgroundAction)];
    [bgView addGestureRecognizer:tap];
    [eView addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(eView);
        make.centerY.equalTo(eView.mas_centerY).offset(config.offsetY);
        make.width.mas_equalTo(eView.frame.size.width - 26.0*2);
    }];
    
    //图片
    UIImageView *imgView = [UIImageView new];
    BOOL isImage = NO;
    if (config.img_name) {
        isImage = YES;
        imgView.image = [UIImage imageNamed:config.img_name];
    } else if (config.image) {
        isImage = YES;
        imgView.image = config.image;
    } else if (config.img_url) {
        isImage = YES;
//        [imgView kj_setImageWithUrl:config.img_url w:0 h:0];
//        imgView setim
    }
    CGSize imgSize = CGSizeMake(0, 0);
    if (config.img_url) {
        imgSize = config.img_size;
    } else {
        if (imgView.image) {
            imgSize = imgView.image.size;
        }
    }
    [bgView addSubview:imgView];
    [imgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView.mas_top).offset(0);
        make.centerX.equalTo(bgView);
        make.size.mas_equalTo(imgSize);
    }];
    
    
    UILabel *label = [UILabel new];
    label.font = config.lb_font;
    label.textColor = config.lb_color;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = config.lb_title;
    label.numberOfLines = 0;
    [bgView addSubview:label];
    [label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgView.mas_bottom).offset(isImage ? config.content_top : 0.f);
        make.left.equalTo(bgView.mas_left).offset (0);
        make.right.equalTo(bgView.mas_right).offset(0);
    }];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    BOOL isButton = [Utility isString:config.bt_title];
    if (isButton) {
        btn.titleLabel.font = config.bt_font;
        [btn setTitle:config.bt_title forState:UIControlStateNormal];
        [btn setTitleColor:config.bt_color forState:UIControlStateNormal];
        [btn addTarget:eView action:@selector(didButtonAction) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.cornerRadius = config.bt_radius;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = config.bt_bgColor;
    }
    [bgView addSubview:btn];
    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(isButton ? config.bt_top : 0.f);
        make.size.mas_equalTo(config.bt_size);
        make.bottom.equalTo(bgView.mas_bottom).offset(0);
        make.centerX.equalTo(bgView.mas_centerX).offset(0);
    }];
    
    if (!isExist) {//如果不存在 则addSubview
        [sView addSubview:eView];
        [sView bringSubviewToFront:eView];
    }
}


/**
 空数据类型展示

 @param emptyType 造成空数据的类型(负数时如果存在则删除，正数时根据下标查找展示的内容并进行显示)
 @param sView superView
 @param btnBlock 点击按钮的回调
 @param bgdBlock 点击背景的回调
*/
+ (void)customEmptyViewType:(int)emptyType
              withSuperView:(UIView *)sView
           withButtonAction:(TapButtonAction)btnBlock
       withBackgroundAction:(TapBackgroundAction)bgdBlock {
    
    //查询sView上是否已经有该view了
    EmptyStateView *eView;
    BOOL isExist = NO;//记录在sView是否已经存在eView，避免重复addSubview
    for (UIView *view in sView.subviews) {
        if ([view isKindOfClass:[EmptyStateView class]]) {
            eView = (EmptyStateView *)view;
            isExist = YES;
            break;
        }
    }
    if (emptyType < 0) {//如果type为负数，则删除显示
        if (eView) {
            [eView removeFromSuperview];
            eView = nil;
        }
        return;
    }
    
    if (eView) {
        if (eView.typeIndex == emptyType) {//如果已经存在并且状态没有改变，则不需要重新创建子视图
            return;
        }
        //如果已经存在但是状态改变了，则重新创建子视图
        [eView removeAllSubviews];
    } else {
        //如果不存在  则创建
        eView = [[EmptyStateView alloc] initWithFrame:CGRectMake(0, 0, sView.width, sView.height)];
        eView.backgroundColor = [UIColor clearColor];
    }

    eView.backgroundColor = [UIColor whiteColor];
    eView.tapBtnBlock = btnBlock;//点击按钮的回调
    eView.tapBgdBlock = bgdBlock;//点击背景的回调
    eView.typeIndex = emptyType;//记录状态，用于第二次创建做判断是否重新创建子视图
    
    //根据状态emptyType取出子视图要显示的内容
    NSDictionary *typeObject = [EmptyStateView getEmptyViewType:emptyType];
    
    if (typeObject[@"color"]) {
        eView.backgroundColor = typeObject[@"color"];
    }
    CGFloat topSpace = -44;
    if (typeObject[@"topSpace"]) {
        NSNumber * space  = typeObject[@"topSpace"];
        topSpace = space.floatValue;
    }
    
    //子视图的背景view
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:eView action:@selector(didTapBackgroundAction)];
    [bgView addGestureRecognizer:tap];
    [eView addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(eView);
        make.centerY.equalTo(eView.mas_centerY).offset(topSpace);
        make.width.mas_equalTo(eView.frame.size.width - 26.0*2);
    }];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:typeObject[@"image"]];
    [bgView addSubview:imgView];
    [imgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView.mas_top).offset(0);
        make.centerX.equalTo(bgView);
        make.height.mas_equalTo(imgView.image ? imgView.image.size.height : 0.f);
    }];

    
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:15.0];
    label.textColor = UIColorFromRGB(0x666666);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = typeObject[@"content"];
    label.numberOfLines = 0;
    [bgView addSubview:label];
    CGFloat content_top = 10.0;
    if (typeObject[@"content_top"]) {
        content_top = [typeObject[@"content_top"] doubleValue];
    }
    [label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgView.mas_bottom).offset(imgView.image ? content_top : 0.f);
        make.left.equalTo(bgView.mas_left).offset (0);
        make.right.equalTo(bgView.mas_right).offset(0);
    }];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (typeObject[@"button"]) {
        btn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [btn setTitle:typeObject[@"button"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:eView action:@selector(didButtonAction) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.cornerRadius = 2.0;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = [UIColor redColor];
    }
    [bgView addSubview:btn];
    CGFloat button_top = 10.0;
    if (typeObject[@"button_top"]) {
        button_top = [typeObject[@"button_top"] doubleValue];
    }
    CGFloat button_height = 36.0;
    if (typeObject[@"button_height"]) {
        button_height = [typeObject[@"button_height"] doubleValue];
    }
    CGFloat button_width = 126.0;
    if (typeObject[@"button_width"]) {
        button_width = [typeObject[@"button_width"] doubleValue];
    }
    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(typeObject[@"button"] ? button_top : 0.f);
        make.height.mas_equalTo(typeObject[@"button"] ? button_height : 0.f);
        make.width.mas_equalTo(button_width);
        make.bottom.equalTo(bgView.mas_bottom).offset(0);
        make.centerX.equalTo(bgView.mas_centerX).offset(0);
    }];
    
    if (!isExist) {//如果不存在 则addSubview
        [sView addSubview:eView];
    }
}


- (void)didButtonAction {
    if (self.tapBtnBlock) {
        self.tapBtnBlock();
    }
}

- (void)didTapBackgroundAction {
    if (self.tapBgdBlock) {
        self.tapBgdBlock();
    }
}

/**
 获取显示内容
 支持字段:   content、
            image-图片的名字字符串、
            button-按钮的文案、color-UIColor对象、
            topSpace-整体向上偏移多少(默认向上偏移44)、
            button_height-按钮的高度、
            button_width-按钮的宽度、
            content_top-文案顶部和图片底部的距离(当图片没有时，无效)、
            button_top-按钮顶部和文案底部的距离(当文案没有时，无效)
    例如：
        @{@"content":xxx} - 就是纯文字展示
        @{@"content":xxx,@"image":xxx} - 就是图片和文字的结合展示(图片在上，文案在下)
        @{@"content":xxx,@"image:xxx,@"button":@"按钮上的文案"} - 图片 文字 按钮的结合展示(图片在上 文案居中 按钮在下)
    有几个字段就显示几个，但是同一个字段只能有一个
 */
+ (NSDictionary *)getEmptyViewType:(NSInteger)type {
    
    NSArray *typeArray = @[@{@"image":@"btn_network",
                             @"content":@"网络不给力，刷新重试"
                             }, //0 无网络
                           @{@"image":@"btn_empty",
                             @"content":@"暂无记录"
                             },//1 空数据
                           @{@"image":@"btn_nomsg",
                             @"content":@"暂无消息"
                             },//2 空消息
                           
                           ];
    /*
    NSArray *typeArray = @[
     
                           @{@"content":@"暂无符合条件的活动，点击右下方[发起活动]按钮，发布第一个活动吧~", @"topSpace":@(-80)}, //0
                           @{@"content":@"需要开启地理位置权限，才能开启今天谁有空哦~",@"button":@"立即开启", @"topSpace":@(-80)},//1
                           @{@"content":@"暂无即将开始的活动"}, //2
                           @{@"content":@"暂无参与过的活动"}, //3
                           @{@"content":@"暂无已失效的活动"}, //4
                           @{@"content":@"暂未发布日记"}, //5
                           @{@"content":@"暂时没有想去的活动",@"image":@"dummyStatus_wantgo"}, //6
                           @{@"content":@"暂无活动",@"image":@"btn_network"}, //7
                           @{@"content":@"暂时还没有人来",@"image":@"dummyStatus_LookMine"}, //8
                           @{@"content":@"你还没有看过任何人",@"image":@"dummyStatus_LookOthers"}, //9
                           @{@"content":@"暂无互相关注的好友"}, //10
                           @{@"content":@"暂无关注任何人"}, //11
                           @{@"content":@"暂无粉丝"}, //12
                           @{@"content":@"暂无加入群聊"}, //13
                           @{@"content":@"TA隐藏了自己的想去,你看不到啦~"}, //14
                           @{@"content":@"TA隐藏了自己的活动记录,你看不到啦~"}, //15
                           @{@"content":@"TA隐藏了自己的来访记录,你看不到啦~"}, //16
                           @{@"content":@"TA隐藏了自己的关注,你看不到啦~"}, //17
                           @{@"content":@"TA隐藏了自己的粉丝,你看不到啦~"}, //18
                           @{@"content":@"TA隐藏了自己的群组,你看不到啦~"}, //19
                           @{@"content":@"暂无发布的攻略", @"image":@"dummyStatus_guide"}, //20
                           @{@"content":@"暂无发布的活动", @"image":@"dummyStatus_event"}, //21
                           @{@"content":@"暂无关注任何主办方"}, //22
                           @{@"content":@"暂无关注任何媒体号"}, //23
                           @{@"content":@"开启定位服务，发现附近日记",@"image":@"dummyStatus_place",@"button":@"开启定位",@"color":[UIColor whiteColor]},//24
                           @{@"content":@"点击底栏[发活动]按钮，发布第一个活动~",@"image":@"dummyStatus_invite"},//25
                           @{@"content":@"你还没有加入任何小组喔",@"image":@"dummyStatus_meGroup"},//26
                           @{@"content":@"暂未登录，加入JOIN和我们一起玩乐",@"image":@"dummyStatus_mess",@"button":@"立即登录",@"color":[UIColor clearColor]}, // 27
                           @{@"content":@"暂无消息",@"image":@"dummyStatus_mess",@"color":[UIColor clearColor]}, // 28
                           @{@"content":@"暂无评论喔",@"image":@"dummyStatus_mess"}, // 29
                           @{@"content":@"暂无赞与收藏",@"image":@"dummyStatus_upvote"}, // 30
                           @{@"content":@"暂无新粉丝",@"image":@"dummyStatus_fans"}, // 31
                           @{@"content":@"你尚未发布日记"}, // 32
                           @{@"content":@"你尚未发布讨论"}, // 33
                           @{@"content":@"你尚未发布话题"}, // 34
                           @{@"content":@"你尚未发布攻略"}, // 35
                           @{@"content":@"你尚未发布回复"},  // 36
                           @{@"content":@"暂未收藏日记",@"image":@"dummyStatus_feed"}, // 37
                           @{@"content":@"暂未收藏攻略",@"image":@"dummyStatus_guide"}, // 38
                           @{@"content":@"暂时没有想去的好店",@"image":@"dummyStatus_wantgo"}, //39
                           @{@"content":@"网络连接失败，请刷新或检查网络",@"image":@"dummyStatus_netWork",@"button":@"刷新",@"color":[UIColor clearColor]}, // 40
                           @{@"content":@"关注好友后，你可以在这里看到对方的心情",@"image":@"dummyStatus_follow",@"topSpace":@(0)}, // 41
                           @{@"content":@"搜索后，这里会显示搜索结果哦~",@"topSpace":@(-80)}, //42
                           @{@"content":@"暂无搜索结果，换个关键词试试吧~",@"topSpace":@(-80)}, //43
                           @{@"content":@"暂无搜索记录~", @"color":[UIColor whiteColor], @"topSpace":@(0)},//44
                           @{@"content":@"点击底栏[发攻略]按钮，发布第一个攻略~",@"image":@"dummyStatus_guide"},//45
                           @{@"content":@"暂无符合筛选条件的活动~",@"color":[UIColor clearColor]},  // 46
                           @{@"content":@"暂无符合筛选条件好店~",@"color":[UIColor clearColor]},  // 47
                           @{@"content":@"通讯录为空"},  // 48
                           @{@"content":@"通讯录中暂无已加入JOIN的好友~"},  // 49
                           @{@"content":@"开启通讯录权限，看看你的朋友们都在JOIN玩些什么",@"image":@"dummyStatus_meGroup",@"button":@"开启"},  // 50
                           @{@"content":@"点击底栏[发日记]按钮，发布第一个日记~",@"image":@"dummyStatus_feed"}, //51
                           @{@"content":@"暂未搜到任何内容，左右滑动一下试试~",@"topSpace":@(-80)}, //52
                           @{@"content":@"暂无符合条件的日记，点击右下方按钮发布日记吧~", @"topSpace":@(-80)}, //53
                           @{@"content":@"本周暂无热门讨论，去小组发帖就能上热门~", @"color":[UIColor whiteColor], @"topSpace":@(0)}, //54
                           @{@"content":@"需要登录才能开启今天谁有空哦~",@"button":@"立即登录", @"topSpace":@(-80)},//55
                           @{@"content":@"需要使用真人头像，才能开启今天谁有空哦~",@"button":@"立即上传", @"topSpace":@(-80)},//56
                           @{@"content":@"需要设置你的今日状态，才能开启今天谁有空哦~",@"button":@"立即设置", @"topSpace":@(-80)},//57
                           @{@"content":@"需要填写自己的家乡，才能查看老乡的心情哦~",@"button":@"立即完善", @"topSpace":@(-80)},//58
                           @{@"content":@"需要填写自己的生日，才能查看同星座的小伙伴发布的日记哦~",@"button":@"立即完善", @"topSpace":@(-80)},//59
                           @{@"content":@"暂无今天有空的男生哦，明天过来看看吧~"},  // 60
                           @{@"content":@"暂无今天有空的女生哦，明天过来看看吧~"},   // 61
                           @{@"content":@"需要填写自己的行业，才能查看同行的心情哦~",@"button":@"立即完善", @"topSpace":@(-80)},//62
                           @{@"content":@"需要填写自己的学校，才能查看同校的心情哦~",@"button":@"立即完善", @"topSpace":@(-80)},//63
                           @{@"content":@"本城市即将开放\"此刻·群聊\"功能，敬请期待~", @"topSpace":@(-80)},//64
                           @{@"content":@"暂无符合条件的日记，点击下方立即参与按钮发布日记吧~", @"topSpace":@(-80)}, //65
                           @{@"content":@"在这里你可以看到附近小伙伴，同时你也会被其他附近的小伙伴发现。你的在线状态将保持三天。点击下方黄色按钮立即开启附近的人。",@"button":@"立即开启",@"topSpace":@(-80)},//66
                           @{@"content":@"需要开启地理位置权限，才能开启附近的人哦~",@"button":@"立即开启"},//67
                           @{@"content":@"只要登录并完善个人资料必填项，就能发现其他小伙伴~",@"button":@"立即登录并完善"},//68
                           @{@"content":@"只要完善个人资料必填项，就能发现其他小伙伴~",@"button":@"立即完善"},//69
                           @{@"content":@"JOIN是一个真人社交平台，需要上传真实头像，就能发现其他小伙伴~",@"button":@"立即上传"},//70
                           @{@"content":@"只要完善个人资料中的问题\"未来想做些什么\"，就能查看附近的人啦~",@"button":@"立即完善"},//71
                           @{@"content":@"只要填写你的专业，就能查看同专业的小伙伴哦~",@"button":@"立即完善"},//72
                           @{@"content":@"暂无符合条件的小伙伴哦~"},//73
                           @{@"content":@"暂无"},//74
                           @{@"content":@"登录以查看关注的人发布的日记",@"button":@"立即登录", @"topSpace":@(-80)},//75
                           @{@"content":@"数据库中暂无该大学，点击创建",@"button":@"立即创建", @"topSpace":@(-80)},//76
                           @{@"content":@"数据库中暂无该专业，点击创建",@"button":@"立即创建", @"topSpace":@(-80)},//77
                           @{@"content":@"数据库中暂无该初中，点击创建",@"button":@"立即创建", @"topSpace":@(-80)},//78
                           @{@"content":@"数据库中暂无该高中，点击创建",@"button":@"立即创建", @"topSpace":@(-80)},//79
                           @{@"content":@"需要完善家乡信息，才能查看同乡的小伙伴哦",@"button":@"立即完善"},//80
                           @{@"content":@"需要完善学校信息，才能查看同校的小伙伴哦",@"button":@"立即完善"},//81
                           @{@"content":@"需要完善生日信息，才能查看同星座的小伙伴哦",@"button":@"立即完善"},//82
                           @{@"content":@"本小组暂无心情，点击底栏\"晒心情\"按钮发布一个吧~"},//83
                           @{@"content":@"暂未发布心情"},//84
                           @{@"content":@"需要登录，才能查看关注的小伙伴的心情哦~",@"button":@"立即登录"},//85
                           @{@"content":@"暂无符合条件的广播，点击右下角按钮发布第一个心情吧~"},//86
                           @{@"content":@"需要填写自己的学校，才能查看同校的心情哦~",@"button":@"立即完善"},//87
                           @{@"content":@"视频处理失败~"},//88
                           @{@"content":@"暂未加入任何话题~"},//89
                           @{@"content":@"暂未发布任何心情~"},//90
                           @{@"content":@"该话题暂无心情，点击右下方按钮发布第一个心情吧~"},//91
                           @{@"content":@"暂无符合条件的心情，点击右下方按钮发布心情吧~"},//92
                           @{@"content":@"暂未搜到任何内容",@"topSpace":@(-80)}, //93
                           @{@"content":@"啊哦~ 暂时没有可用的网络~",@"topSpace":@(0),@"image":@"",@"button_width":@(SCREEN_WIDTH-38.0*2),@"button_height":@42.0,@"content_top":@23.0, @"button_top":@28.0}, //94
                           ];*/
    
    return typeArray[type];
}


@end
