//
//  EmptyConfigure.m
//  Join
//
//  Created by JOIN iOS on 2018/8/17.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import "EmptyConfigure.h"

@implementation EmptyConfigure

- (instancetype)init {
    self = [super init];
    if (self) {
        
        self.color = UIColorFromRGB(0xf2f2f2);
        self.offsetY = -44.0;
        self.lb_font = [UIFont systemFontOfSize:20.0];
        
        self.lb_color = UIColorFromRGB(0x000000);
        self.bt_font = [UIFont systemFontOfSize:15.0];
        
        self.bt_color = UIColorFromRGB(0xFFFFFF);
        self.bt_bgColor = UIColorFromRGB(0x3377FF);
        self.bt_radius = 2.0;
        self.content_top = 32.0;
        self.bt_top = 36.0;
        self.bt_size = CGSizeMake(126.0, 36.0);
        self.img_size = CGSizeMake(60.0, 60.0);
    }
    return self;
}

+ (instancetype)new {
    return [[self alloc] init];
}

@end
