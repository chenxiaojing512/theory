//
//  XJEmptyView.h
//  Loan
//
//  Created by 陈晓晶 on 2019/5/12.
//  Copyright © 2019 陈晓晶. All rights reserved.
//

#import <QMUIKit/QMUIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XJEmptyView : QMUIEmptyView

- (instancetype)initAction:(void(^)(int code ,XJEmptyView *v))block;

/// 重新设置superView
- (void)resetView:(UIView *)sView;
/// 重新设置superView并设置错误码
- (void)resetView:(UIView *)sView code:(int)code;
/// 设置错误码，来自动更新UI样式
@property (assign, nonatomic) int errorCode;
/// 按钮单击事件
@property (copy, nonatomic) void (^ clickAction)(int code, XJEmptyView *v);

@end

NS_ASSUME_NONNULL_END
