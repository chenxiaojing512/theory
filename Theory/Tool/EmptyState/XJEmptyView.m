//
//  XJEmptyView.m
//  Loan
//
//  Created by 陈晓晶 on 2019/5/12.
//  Copyright © 2019 陈晓晶. All rights reserved.
//

#import "XJEmptyView.h"

@implementation XJEmptyView

- (instancetype)initAction:(void(^)(int code ,XJEmptyView *v))block {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.clickAction = block;
    }
    return self;
}


/// 重新设置superView并设置错误码
- (void)resetView:(UIView *)sView code:(int)code {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetView:sView];
        self.errorCode = code;
    });
}

/// 重新设置superView
- (void)resetView:(UIView *)sView {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (sView != self.superview) {
            [self removeFromSuperview];
        }
        [sView addSubview:self];
        [self mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(sView);
        }];
        [sView bringSubviewToFront:self];
        
        [self.actionButton addTarget:self
                              action:@selector(didAction)
                    forControlEvents:UIControlEventTouchUpInside];
    });
}

- (void)didAction {
    if (self.clickAction != nil) {
        self.clickAction(_errorCode, self);
    }
}

/// 默认设置
- (void)normalConfig {
    // title字体
    self.textLabelFont = [UIFont systemFontOfSize:13.0];
    // title颜色
    self.textLabelTextColor = [UIColor redColor];
    // 按钮字体
    self.actionButtonFont = [UIFont systemFontOfSize:15.0];
    // 按钮文字颜色
    self.actionButtonTitleColor = UIColorFromRGB(0x887667);
    // 边框
    self.actionButton.layer.borderColor = UIColor.blueColor.CGColor;
    self.actionButton.layer.borderWidth = 1.0;
    // 隐藏菊花
    [self setLoadingViewHidden:YES];
    
    self.actionButton.contentEdgeInsets = UIEdgeInsetsMake(5, 8, 5, 8);
    
    // 默认白色
    self.backgroundColor = UIColor.whiteColor;
    
}

/// 设置错误码，自动更新UI展示
- (void)setErrorCode:(int)errorCode {
    if (_errorCode != errorCode) {
        _errorCode = errorCode;
        //因为code不一致，需要改变样式
        [self updateEmpty];
    }
}

/// 这里根据不同的错误码，来显示不同的样式
- (void)updateEmpty {
    //恢复默认设置，如果在设置错误码过后，有需要修改样式，则外部直接修改即可
    [self normalConfig];
    // 让其显示到最上层
    [self.superview bringSubviewToFront:self];
    // 如果错误码事200，则隐藏
    self.hidden =  _errorCode == 200 || _errorCode == 1;
    // 根据错误码来设置默认显示。如果有title/按钮/图片等需要隐藏，文案设置为nil即可，会自动隐藏
    switch (_errorCode) {
        case 200:
            self.hidden = YES;
            break;
        case 404:
            [self setTextLabelText:@"哎呀，页面走丢了..."];
            [self setActionButtonTitle:@"刷新"];
            [self setImage:[UIImage imageNamed:@""]];
            break;
            
        default:
            break;
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.actionButton.layer.cornerRadius = self.actionButton.qmui_height / 2.0;
}

@end
