//
//  KJNetworkGlobalConfigs.h
//  KJNetworkRequest
//
//  Created by 黄克瑾 on 2020/6/19.
//  Copyright © 2020 黄克瑾. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface KJNetworkGlobalConfigs : NSObject

+ (instancetype)defaultConfigs;

/// 全局参数配置
@property (nonatomic, strong) NSMutableDictionary *kjParams;

/// 全局Header配置
@property (nonatomic, strong) NSMutableDictionary *kjHeader;

/// 设置HOST
@property (nonatomic, copy) NSString *kjHost;

@end

NS_ASSUME_NONNULL_END
