//
//  DKUserHelper.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKUserHelper.h"

@implementation DKUserHelper
+(void)saveValue:(id) value forKey:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value requiringSecureCoding:YES error:nil];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:key];
    [userDefaults synchronize];
}

+(id)valueWithKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:key];
    id user;
    if(data){
//        user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        user = [NSKeyedUnarchiver unarchivedObjectOfClass:[DKUserHelper class] fromData:data error:nil];
    }
    
    return user;
}

+(BOOL)boolValueWithKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:key];
}

+(void)removeValueWithKey:(NSString *) key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
    
}

+(void)saveBoolValue:(BOOL)value withKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:value forKey:key];
    [userDefaults synchronize];
}

+(void)print{
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSDictionary *dic = [userDefaults dictionaryRepresentation];
    //    DNSLog(@"%@",dic);
}

@end
