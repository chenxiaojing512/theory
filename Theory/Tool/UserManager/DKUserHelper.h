//
//  DKUserHelper.h
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKUserHelper : NSObject
+(void)saveValue:(id) value forKey:(NSString *)key;

+(id)valueWithKey:(NSString *)key;

+(BOOL)boolValueWithKey:(NSString *)key;

+(void)saveBoolValue:(BOOL)value withKey:(NSString *)key;

+(void)removeValueWithKey:(NSString *) key;
+(void)print;
@end

NS_ASSUME_NONNULL_END
