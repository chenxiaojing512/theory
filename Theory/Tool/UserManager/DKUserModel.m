//
//  DKUserModel.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKUserModel.h"

@implementation DKUserModel

-(id)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        //昵称
        if ([[dictionary objectForKey:@"nickname"] isKindOfClass:[NSNull class]]) {
            self.userName = @"未命名";
        }else{
            self.userName = [dictionary objectForKey:@"nickname"];
        }
        //手机号
        if ([[dictionary objectForKey:@"mobile"] isEqualToString:@""]) {
            self.userPhone = @"";
        }else{
            self.userPhone = [dictionary objectForKey:@"mobile"];
        }
        
        //用户ID
        self.userID = [dictionary objectForKey:@"user_id"];
        
        //token
        self.userToken = [dictionary objectForKey:@"token"];
        //用户头像
        self.headimgurl = [dictionary objectForKey:@"head_pic"];
        //用户性别
        self.userSex = [dictionary objectForKey:@"sex"];
        //用户生日
        self.userAge = [dictionary objectForKey:@"birthday"];
        
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.userName forKey:@"username"];
    [aCoder encodeObject:self.userID forKey:@"user_id"];
    [aCoder encodeObject:self.userPhone forKey:@"mobile"];
    [aCoder encodeObject:self.userToken forKey:@"token"];
    [aCoder encodeObject:self.headimgurl forKey:@"head_pic"];
    
    [aCoder encodeObject:self.userSex forKey:@"sex"];
    [aCoder encodeObject:self.userAge forKey:@"birthday"];
    
    
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.userName = [aDecoder decodeObjectForKey:@"username"];
        self.userID = [aDecoder decodeObjectForKey:@"user_id"];
        
        self.userPhone = [aDecoder decodeObjectForKey:@"mobile"];
        self.userToken = [aDecoder decodeObjectForKey:@"token"];
        self.headimgurl = [aDecoder decodeObjectForKey:@"head_pic"];
        
        self.userAge = [aDecoder decodeObjectForKey:@"sex"];
        self.userSex = [aDecoder decodeObjectForKey:@"birthday"];
        
    }
    return self;
}


-(NSString *)description{
    NSString *format = @"{\n"
    "    a_userID = %@;\n"
    "    a_userName= %@;\n"
    "    a_userPhone = %@;\n"
    "    a_usertoken = %@;\n"
    "    a_headImgUrl = %@;\n"
    "    a_userSex = %@;\n"
    "    a_userAge = %@;\n"
    
    
    "}";
    
    return [NSString stringWithFormat:format,
            self.userID,
            self.userName,
            self.userPhone,
            self.userToken,
            self.headimgurl,
            self.userSex,
            self.userAge];
            
    
}
@end
