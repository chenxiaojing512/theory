//
//  DKUserInfoManager.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKUserInfoManager.h"
#define NUD_USER_INFROMATION @"NUD_USER_INFROMATION"
static DKUserInfoManager *sharedManager = nil;
@implementation DKUserInfoManager


+(DKUserInfoManager *)sharedUserInfoManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc]init];
    });
    
    return sharedManager;
}

-(BOOL)isLoginIn{
    DKUserModel *user = [self getUserInfo];
    if (user) {
        return YES;
    }else{
        return NO;
    }
}

//保存信息
-(void)saveUserInfo:(DKUserModel*)userModel{
    [DKUserHelper saveValue:userModel forKey:NUD_USER_INFROMATION];

}

//获取信息
-(DKUserModel*)getUserInfo{
    DKUserModel *user = [DKUserHelper valueWithKey:NUD_USER_INFROMATION];
    return user;
}

//移除个人信息
-(void)removeUserInfo:(DKUserModel*)userModel{
    [DKUserHelper removeValueWithKey:NUD_USER_INFROMATION];
}


@end
