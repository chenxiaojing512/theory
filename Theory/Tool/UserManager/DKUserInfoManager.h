//
//  DKUserInfoManager.h
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKUserModel.h"
#import "DKUserHelper.h"
NS_ASSUME_NONNULL_BEGIN

@interface DKUserInfoManager : NSObject

+(DKUserInfoManager *)sharedUserInfoManager;

-(BOOL)isLoginIn;

//保存信息
-(void)saveUserInfo:(DKUserModel*)userModel;

//获取信息
-(DKUserModel*)getUserInfo;

//移除个人信息
-(void)removeUserInfo:(DKUserModel*)userModel;

@end

NS_ASSUME_NONNULL_END
