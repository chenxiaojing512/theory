//
//  DKUserModel.h
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKUserModel : NSObject

@property (nonatomic, strong) NSString *userID;//用户ID
@property (nonatomic, strong) NSString *userToken;//用户token


@property (nonatomic, strong) NSString *userName;//用户昵称
@property (nonatomic, strong) NSString *userSex;//用户性别
@property (nonatomic, strong) NSString *userAge;//用户年龄
@property (nonatomic, strong) NSString *userPhone;//用户手机号
@property(nonatomic, strong) NSString *headimgurl;//用户头像



- (id) initWithDictionary:(NSDictionary *)dictionary;
@end

NS_ASSUME_NONNULL_END
