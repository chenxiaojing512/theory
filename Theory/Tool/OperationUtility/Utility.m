//
//  Utility.m
//  Join
//
//  Created by 陈晓晶 on 16/11/28.
//  Copyright © 2016年 huangkejin. All rights reserved.
//

#import "Utility.h"
#import "MBProgressHUD.h"
#import "DLHDActivityIndicator.h"
#import <sys/utsname.h>
//#import "KJNetworkConfig.h"

//#import "KJDialogPureText.h"
//#import "KJDialogActivityText.h"
NSString *const kYFValidateRegexIDCardNumber     = @"(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}(((19|20)[0-9]{2}(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((19|20)(0[48]|[2468][048]|[13579][26])0229)|(20000229))[0-9]{3}[0-9Xx]";
@implementation Utility


/**验证手机号*/
+ (BOOL)isValidTelephoneNum:(NSString *)strPhoneNum {
    NSString *phoneNumRegex1 = @"^1(3[0-9]|4[0-9]|5[0-9]|6[0-9]|7[0-9]|8[0-9]|9[0-9])\\d{8}$";
    NSPredicate *phoneNum1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneNumRegex1];
    
    return [phoneNum1 evaluateWithObject:strPhoneNum];
}

#pragma mark -提示语
+ (void)showTextDialog:(UIView *)view {
    //初始化进度框，置于当前的View当中
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:HUD];
    
    //如果设置此属性则当前的view置于后台
    //    HUD.dimBackground = YES;
    
    //设置对话框文字
    HUD.label.text = @"请稍等";
    
    //显示对话框
//    [HUD showAnimated:YES whileExecutingBlock:^{
//        //对话框显示时需要执行的操作
//        sleep(3);
//    } completionBlock:^{
//        //操作执行完后取消对话框
//        [HUD removeFromSuperview];
//    }];
    [HUD setMinShowTime:3];
    [HUD showAnimated:YES];
}

+ (void)showProgressDialog:(UIView *)view {
    [DLHDActivityIndicator hideActivityIndicatorInView:view];
    DLHDActivityIndicator *indicator = [[DLHDActivityIndicator alloc] init];
    indicator.overlayFontColor = [UIColor whiteColor];
    indicator.overlayFont = [UIFont systemFontOfSize:15.f];
    indicator.overlayTintColor = [UIColor blackColor];
    indicator.activityColor = [UIColor whiteColor];
    indicator.window = view;
    [indicator showWithLabelText:@"正在加载"];
}

+ (void)showProgressDialog:(UIView *)view text:(NSString *)text {
    
    [DLHDActivityIndicator hideActivityIndicatorInView:view];
    DLHDActivityIndicator *indicator = [[DLHDActivityIndicator alloc] init];
    indicator.overlayFontColor = [UIColor whiteColor];
    indicator.overlayFont = [UIFont systemFontOfSize:15.f];
    indicator.overlayTintColor = [UIColor blackColor];
    indicator.activityColor = [UIColor whiteColor];
    indicator.window = view;
    [indicator showWithLabelText:text];
}

+ (void)showProgressDialogText:(NSString *)text {
    DLHDActivityIndicator *indicator = [DLHDActivityIndicator shared];
    indicator.overlayFontColor = [UIColor whiteColor];
    indicator.overlayFont = [UIFont systemFontOfSize:15.f];
    indicator.overlayTintColor = [UIColor blackColor];
    indicator.activityColor = [UIColor whiteColor];
    [indicator showWithLabelText:text];
}

+ (void)hideProgressDialog:(UIView *)view {
    [DLHDActivityIndicator hideActivityIndicatorInView:view];
    
}

+ (void)hideProgressDialog {
    
    [DLHDActivityIndicator hideActivityIndicator];
}

//+ (void)showCustomDialog:(UIView *)view title:(NSString *)text {
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
//    [view addSubview:HUD];
//    HUD.labelText = text;
//    HUD.labelFont = [UIFont systemFontOfSize:14.0f];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
//    HUD.contentColor = [UIColor whiteColor];
//    HUD.mode = MBProgressHUDModeCustomView;
//    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"save_complete_icon"]];
//    [HUD showAnimated:YES whileExecutingBlock:^{
//        sleep(2);
//    } completionBlock:^{
//        [HUD removeFromSuperview];
//    }];
//}

+ (void)showAllTextDialog:(UIView *)view  Text:(NSString *)text{
    if (!view) {
        return;
    }
    if ([view isKindOfClass:[UITableView class]]) {
        view = view.superview;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
        [view addSubview:HUD];
        HUD.margin = 12.f;
        HUD.label.text = text;
        HUD.label.font = [UIFont systemFontOfSize:14.0];
        HUD.bezelView.color = [UIColor blackColor];
        HUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        HUD.mode = MBProgressHUDModeText;
        HUD.contentColor = [UIColor whiteColor];
        [HUD layoutIfNeeded];
        [HUD showAnimated:YES];
        [HUD hideAnimated:YES afterDelay:1.5];
    });
}


//计算一个时间与当前时间的时间差 返回秒
+ (NSDateComponents *)difftimeDate:(NSDate *)date withUnit:(NSCalendarUnit)unit {
    if (!date) {
        date = [NSDate date];
    }
    // 获取当前日历
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // 需要对比的时间数据
//    NSCalendarUnit unit = unit;//NSCalendarUnitYear | NSCalendarUnitMonth
    //| NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    // 对比时间差
    NSDateComponents *dateCom = [calendar components:unit fromDate:date toDate:[NSDate date] options:0];
    
    /**
     *年差额 = dateCom.year
     *月差额 = dateCom.month
     *日差额 = dateCom.day
     *小时差额 = dateCom.hour
     *分钟差额 = dateCom.minute
     *秒差额 = dateCom.second
     */
    return dateCom;
}

+ (NSDateComponents *)date:(NSDate *)currenDate subtractDate:(NSDate *)date withUnit:(NSCalendarUnit)unit {
    // 获取当前日历
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // 需要对比的时间数据
//    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth
//    | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    // 对比时间差
    NSDateComponents *dateCom = [calendar components:unit fromDate:currenDate toDate:date options:0];
    
    /**
     *年差额 = dateCom.year
     *月差额 = dateCom.month
     *日差额 = dateCom.day
     *小时差额 = dateCom.hour
     *分钟差额 = dateCom.minute
     *秒差额 = dateCom.second
     */
    return dateCom;
}

//获取当前控制器
//+ (UIViewController *)getCurrentViewController {
//
//    UIViewController *resultVC;
//    resultVC = [Utility _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
//    while (resultVC.presentedViewController) {
//        resultVC = [Utility _topViewController:resultVC.presentedViewController];
//    }
//    return resultVC;
//}

//+ (UIViewController *)_topViewController:(UIViewController *)vc {
//    if ([vc isKindOfClass:[UINavigationController class]]) {
//        return [Utility _topViewController:[(DKNavigationController *)vc topViewController]];
//    } else if ([vc isKindOfClass:[UITabBarController class]]) {
//        return [Utility _topViewController:[(MainTabBarController *)vc selectedViewController]];
//    } else {
//        return vc;
//    }
//    return nil;
//}

//网络监测
/*
+ (void)reachabilityStatusChange {
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态发生改变的时候调用这个block
        //通知整个项目 ，网络发生变化 status=1(手机网络) status=2(wifi) status<1表示网络不能用
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:@"kAFNReachabilityStatusChangeNtf" object:@(status)];
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWiFi:
                Plog(@"WIFI");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                Plog(@"自带网络");
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
                Plog(@"没有网络");
                break;
                
            case AFNetworkReachabilityStatusUnknown:
                Plog(@"未知网络");
                break;
            default:
                Plog(@"未知");
                break;
        }
    }];
    // 开始监控
    [mgr startMonitoring];
}*/

//字典转json字符串
+ (NSString *)dictionaryToJson:(NSDictionary *)dic {
    NSError *parseError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

//json字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
    }
    
    return dic;
}
//数组转json字符串
+ (NSString *)arrayToJsonString:(NSArray *)array {
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

//比较两个颜色是否相同
+ (BOOL)color:(UIColor*)firstColor equalTo:(UIColor*)secondColor {
    if (CGColorEqualToColor(firstColor.CGColor, secondColor.CGColor)) {
        return YES;
    } else {
        return NO;
    }
}

//传入 秒  得到  xx分钟xx秒
+ (NSString *)getMMSSFromSS:(NSInteger)seconds {
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    
    return format_time;
    
}



/**获取设备型号*/
+ (NSString *)getMachine {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine
                                            encoding:NSASCIIStringEncoding];
    return platform;
    
}

/**获取手机型号*/
+ (UtilityIphoneModels)iphoneModels {
    
     [[UIDevice currentDevice] identifierForVendor];
    
    NSString *platform = [self getMachine];
    if ([platform isEqualToString:@"iPhone3,1"] ||
        [platform isEqualToString:@"iPhone3,2"] ||
        [platform isEqualToString:@"iPhone3,3"]) {
        //IP4
        return kjIp4;
    } else if ([platform isEqualToString:@"iPhone4,1"]) {
        //IP4S
        return kjIp4s;
    } else if ([platform isEqualToString:@"iPhone5,1"] ||
               [platform isEqualToString:@"iPhone5,2"]) {
        //IP5
        return kjIp5;
    } else if ([platform isEqualToString:@"iPhone5,3"] ||
               [platform isEqualToString:@"iPhone5,4"]) {
        //IP5C
        return kjIp5c;
    } else if ([platform isEqualToString:@"iPhone6,1"] ||
               [platform isEqualToString:@"iPhone6,2"]) {
        //IP5S
        return kjIp5s;
    } else if ([platform isEqualToString:@"iPhone7,1"]) {
        //IP6P
        return kjIp6p;
    } else if ([platform isEqualToString:@"iPhone7,2"]) {
        //IP6
        return kjIp6;
    } else if ([platform isEqualToString:@"iPhone8,1"]) {
        //IP6S
        return kjIp6s;
    } else if ([platform isEqualToString:@"iPhone8,2"]) {
        //IP6SP
        return kjIp6sp;
    } else if ([platform isEqualToString:@"iPhone8,4"]) {
        //IPSE
        return kjIpse;
    } else if ([platform isEqualToString:@"iPhone9,1"] ||
               [platform isEqualToString:@"iPhone9,3"]) {
        //IP7
        return kjIp7;
    } else if ([platform isEqualToString:@"iPhone9,2"] ||
               [platform isEqualToString:@"iPhone9,4"]) {
        //IP7P
        return kjIp7p;
    } else if ([platform isEqualToString:@"iPhone10,1"] ||
               [platform isEqualToString:@"iPhone10,4"]) {
        //IP8
        return kjIp8;
    } else if ([platform isEqualToString:@"iPhone10,2"] ||
               [platform isEqualToString:@"iPhone10,5"]) {
        //IP8P
        return kjIp8p;
    } else if ([platform isEqualToString:@"iPhone10,3"] ||
               [platform isEqualToString:@"iPhone10,6"]) {
        //IPX
        return kjIpx;
    } else if ([platform isEqualToString:@"iPhone11,2"]) {
        //IPXS
        return kjIpxs;
    } else if ([platform isEqualToString:@"iPhone11,8"]) {
        //IPXR
        return kjIpxr;
    } else if ([platform isEqualToString:@"iPhone11,4"] ||
               [platform isEqualToString:@"iPhone11,6"]) {
        //IPXSMAX
        return kjIpxsmax;
    }
    
    if ([platform hasPrefix:@"iPhone"]) {
        return kjIpx;
    }
    return kjNone;
}

/**状态栏高度*/
+ (CGFloat)kjStatusHeight {
    UtilityIphoneModels models = [self iphoneModels];
    if (models == kjIpx ||
        models == kjIpxs ||
        models == kjIpxr ||
        models == kjIpxsmax) {
        return 44.0;
    } else {
        return 20.0;
    }
}

/**系统底部Home条的高度*/
+ (CGFloat)kjHomeIndicatorHeight {
    UtilityIphoneModels models = [self iphoneModels];
    if (models == kjIpx ||
        models == kjIpxs ||
        models == kjIpxr ||
        models == kjIpxsmax) {
        return 34.0;
    } else {
        return 0.f;
    }
}

/**Tabbar高度*/
+ (CGFloat)kjTabbarHeight {
    return [self kjHomeIndicatorHeight] + 49.0;
}


/**导航栏高度*/
+ (CGFloat)kjNaviHeight {
    return 44.0;
}

/**导航栏+状态栏高度*/
+ (CGFloat)kjSNHeight {
    return ([self kjNaviHeight] + [self kjStatusHeight]);
}

/**导航栏+状态栏+Tabbar高度*/
+ (CGFloat)kjSNTHeight {
    return ([self kjSNHeight] + [self kjTabbarHeight]);
}

/**判断状态栏是否是44像素*/
+ (BOOL)if_status44 {
    if ([self kjStatusHeight] == 44.0) {
        return YES;
    } else {
        return NO;
    }
}

/**判断tabbar是否是83像素*/
+ (BOOL)if_tabbar83 {
    if ([self kjTabbarHeight] == 83.0) {
        return YES;
    } else {
        return NO;
    }
}

/**判断是否有HomeIndicator*/
+ (BOOL)isHomeIndicator {
    if ([self kjHomeIndicatorHeight] > 0) {
        return YES;
    }
    return NO;
}


//判断字符串是否存在,如果字符串全是空格或者换行，也算不存在
+ (BOOL) isString:(NSString *)str {
    if (![str isKindOfClass:NSString.class]) {
        return NO;
    }
    //去掉空格
    NSString *currentStr = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    //去掉换行
    currentStr = [currentStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if (!currentStr ||
        [currentStr isEqualToString:@""] ||
        [currentStr isEqualToString:@"(null)"] ||
        [currentStr isEqualToString:@"<null>"]) {
        return NO;
    }
    return YES;
}

+(NSString *)UIImageToBase64Str:(UIImage *) image
{
    NSData *data = UIImageJPEGRepresentation(image, 1.0f);
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return encodedImageStr;
}
+ (NSData *)imageTransFormData:(UIImage *)image
{
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    //几乎是按0.5图片大小就降到原来的一半
    return imageData;
}


//计算高度
+(CGFloat)calculationStringHeight:(NSDictionary*)fontDic width:(CGSize)width string:(NSString*)string{
 
    CGFloat cellHeight = [string boundingRectWithSize:width options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:fontDic context:nil].size.height;

    return cellHeight;
    
}

//计算宽度
+(CGFloat)calculationStringWidth:(NSDictionary*)fontDic width:(CGSize)width string:(NSString*)string{

    CGFloat cellWidth = [string boundingRectWithSize:width options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:fontDic context:nil].size.width;
    
    return cellWidth;
}

//银行卡展示格式化
+(NSString *)getNewStarBankNumWitOldNum:(NSString*)num{
    NSString *bankNum = num;
    NSMutableString *mutableStr;
    if (bankNum.length) {
        mutableStr = [NSMutableString stringWithString:bankNum];
        for (int i = 0 ; i < mutableStr.length; i ++) {
            if (i>3&&i<mutableStr.length - 4) {
                [mutableStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
            }
        }
        NSString *text = mutableStr;
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        return newString;
    }
    return bankNum;
}


//是否支持
+(BOOL)isSupport:(LAContext *)context {
    if([UIDevice currentDevice].systemVersion.doubleValue >= 8.0){
        //能否使用TouchID/FaceID识别
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]) {
            return YES;
        }else {
            NSLog(@"请确保(5S以上机型),TouchID未打开");
            return NO;
        }
    }else {
        NSLog(@"此设备不支持TouchID/FaceID识别");
        return NO;
    }
}





//根据卡号判断银行
+ (NSString *)returnBankName:(NSString *)cardName {
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"bank" ofType:@"plist"];
    NSDictionary *resultDic = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSArray *bankBin = resultDic.allKeys;
    if (cardName.length < 6) {
        return @"";
    }
    NSString *cardbin_6 ;
    if (cardName.length >= 6) {
        cardbin_6 = [cardName substringWithRange:NSMakeRange(0, 6)];
    }
    NSString *cardbin_8 = nil;
    if (cardName.length >= 8) {
        //8位
        cardbin_8 = [cardName substringWithRange:NSMakeRange(0, 8)];
    }
    if ([bankBin containsObject:cardbin_6]) {
        return [resultDic objectForKey:cardbin_6];
    } else if ([bankBin containsObject:cardbin_8]){
        return [resultDic objectForKey:cardbin_8];
    } else {
        return @"";
    }
    return @"";
    
}

+(NSString*)returnTelPhone:(NSString*)string{
    NSString *tel= @"";
    
    NSMutableArray *temps = [NSMutableArray array];
    for(int i =0; i < [string length]; i++){
        [temps addObject:[string substringWithRange:NSMakeRange(i,1)]];
      }
    //15313110883
    for (NSInteger i = 0; i<temps.count; i++) {
        if (i == 3 || i == 7) {
            tel = [tel stringByAppendingString:@" "];
        }
        tel = [tel stringByAppendingString:temps[i]];
    }
    
    return tel;
}


+(NSString*)returnMoney:(NSString*)string{
    
    
    NSString *tel= @"";
    
    NSMutableArray *temps = [NSMutableArray array];
    for(int i =0; i < [string length]; i++){
        [temps addObject:[string substringWithRange:NSMakeRange(i,1)]];
    }
    //15313110883
    for (NSInteger i = 0; i<temps.count; i++) {
        if (i == 3 || i == 7) {
            tel = [tel stringByAppendingString:@","];
        }
        tel = [tel stringByAppendingString:temps[i]];
    }
    
    return tel;
}




//判断是否含有非法字符 yes 有  no没有
+ (BOOL)JudgeTheillegalCharacter:(NSString *)content{
    //提示 标签不能输入特殊字符
    NSString *str =@"^[A-Za-z0-9\\u4e00-\u9fa5]+$";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", str];
    if (![emailTest evaluateWithObject:content]) {
        return YES;
    }
    return NO;
}


//获取当前时间
+(NSString*)getCurrentDate{
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYYMM"];
    NSString *DateTime = [formatter stringFromDate:date];
    return DateTime;
}


//获取下个月的时间
+(NSString*)getNextMonth{
    
    //得到当前的时间
    NSDate * mydate = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYYMM"];
    NSString *currentDateStr = [dateFormatter stringFromDate:[NSDate date]];
    //                NSLog(@"---当前的时间的字符串 =%@",currentDateStr);
    
    //创建日历
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = nil;
    comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitMonth fromDate:mydate];
    NSDateComponents *adcomps = [[NSDateComponents alloc] init];
    [adcomps setYear:0];   //年
    [adcomps setMonth:1];//月  （-3 代表 三个月前的一天）
    [adcomps setDay:0];     //日
    NSDate *newdate = [calendar dateByAddingComponents:adcomps toDate:mydate options:0];
    NSString *beforDate = [dateFormatter stringFromDate:newdate];
    return beforDate;
}

+ (NSString *)getDealNumwithstring:(NSString *)string withNumCount:(NSInteger)integer{
    
    
    NSDecimalNumber *numberA = [NSDecimalNumber decimalNumberWithString:string];
    NSDecimalNumber *numberB ;
    if (integer == 4) {
        numberB =  [NSDecimalNumber decimalNumberWithString:@"10000"];
    } else if (integer == 8){
        numberB =  [NSDecimalNumber decimalNumberWithString:@"100000000"];
    }else{
        numberB =  [NSDecimalNumber decimalNumberWithString:@"10000"];
    }
    //NSDecimalNumberBehaviors对象的创建  参数 1.RoundingMode 一个取舍枚举值 2.scale 处理范围 3.raiseOnExactness  精确出现异常是否抛出原因 4.raiseOnOverflow  上溢出是否抛出原因  4.raiseOnUnderflow  下溢出是否抛出原因  5.raiseOnDivideByZero  除以0是否抛出原因。
    NSDecimalNumberHandler *roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    /// 这里不仅包含Multiply还有加 减 乘。
    NSDecimalNumber *numResult = [numberA decimalNumberByDividingBy:numberB withBehavior:roundingBehavior];
    NSString *strResult = [numResult stringValue];
    return strResult;
}


+(BOOL)isBlankDiction:(NSDictionary *)theDict
{
    if (theDict == nil||[theDict isEqual:[NSNull null]])
    {
        return YES;
    }
    if ([theDict isKindOfClass:[NSDictionary class]])
    {
        if ([theDict count] == 0)
        {
            return YES;
        }
    }
    return NO;
}

//判断字符串是否为空或者空格或者类型不符
+(BOOL)isBlankString:(NSString *)string
{
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([string isKindOfClass:[NSString class]])
    {
        if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        {
            return YES;
        }
        if ([string isEqualToString:@"(null)"] || [string isEqualToString:@"<null>"])
        {
            return YES;
        }
    }
    return NO;
}


+(BOOL)isBlankArray:(NSArray *)theArray
{
    if (theArray == nil||[theArray isEqual:[NSNull null]])
    {
        return YES;
    }
    if ([theArray isKindOfClass:[NSArray class]])
    {
        if ([theArray count] == 0)
        {
            return YES;
        }
    }
    return NO;
}


+(BOOL)isBlankMuArray:(NSArray *)theArray
{
    if (theArray == nil||[theArray isEqual:[NSNull null]])
    {
        return YES;
    }
    if ([theArray isKindOfClass:[NSMutableArray class]])
    {
        if ([theArray count] == 0)
        {
            return YES;
        }
    }
    return NO;
}


//对数组随机排序
+ (NSArray *)sortedRandomArrayByArray:(NSArray *)array{
    
    NSArray *randomArray = [[NSArray alloc]init];
    randomArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        int seed = arc4random_uniform(2);
        if (seed) {
            return [str1 compare:str2];
        } else {
            return [str2 compare:str1];
        }
    }];
    
    return randomArray;
}


#pragma mark 验证身份证是否合法
/**
 * 功能:验证身份证是否合法
 * 参数:输入的身份证号
 */
+ (BOOL)isIdCard:(NSString *)sPaperId
{
    //判断位数
    if ([sPaperId length] < 15 ||[sPaperId length] > 18) {
        return NO;
    }
    NSString *carid = sPaperId;
    long lSumQT =0;
    //加权因子
    int R[] ={7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
    //校验码
    unsigned char sChecker[11]={'1','0','X', '9', '8', '7', '6', '5', '4', '3', '2'};
    //将15位身份证号转换成18位
    NSMutableString *mString = [NSMutableString stringWithString:sPaperId];
    if ([sPaperId length] == 15) {
        [mString insertString:@"19" atIndex:6];
        long p = 0;
        const char *pid = [mString UTF8String];
        for (int i=0; i<=16; i++)
        {
            p += (pid[i]-48) * R[i];
        }
        int o = p%11;
        NSString *string_content = [NSString stringWithFormat:@"%c",sChecker[o]];
        [mString insertString:string_content atIndex:[mString length]];
        carid = mString;
        
    }
    //判断地区码
    NSString * sProvince = [carid substringToIndex:2];
    
    if (![self areaCode:sProvince]) {
        
        return NO;
    }
    //年份
    int strYear = [[carid substringWithRange:NSMakeRange(6, 4)]intValue];
    //月份
    int strMonth = [[carid substringWithRange:NSMakeRange(10, 2)]intValue];
    //日
    int strDay = [[carid substringWithRange:NSMakeRange(12, 2)]intValue];
    
    NSTimeZone *localZone = [NSTimeZone localTimeZone];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeZone:localZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:[NSString stringWithFormat:@"%d-%d-%d 12:01:01",strYear,strMonth,strDay]];
    if (date == nil) {
        
        return NO;
    }
    
    const char *PaperId  = [carid UTF8String];
    
    //检验长度
    if( 18 != strlen(PaperId)) return -1;
    //校验数字
    for (int i=0; i<18; i++)
    {
        if ( !isdigit(PaperId[i]) && !(('X' == PaperId[i] || 'x' == PaperId[i]) && 17 == i) )
        {
            return NO;
        }
    }
    //验证最末的校验码
    for (int i=0; i<=16; i++)
    {
        lSumQT += (PaperId[i]-48) * R[i];
    }
    if (sChecker[lSumQT%11] != PaperId[17] )
    {
        return NO;
    }
    
    return YES;
}


/**
 * 功能:判断是否在地区码内
 * 参数:地区码
 */
+(BOOL)areaCode:(NSString *)code
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@"北京" forKey:@"11"];
    [dic setObject:@"天津" forKey:@"12"];
    [dic setObject:@"河北" forKey:@"13"];
    [dic setObject:@"山西" forKey:@"14"];
    [dic setObject:@"内蒙古" forKey:@"15"];
    [dic setObject:@"辽宁" forKey:@"21"];
    [dic setObject:@"吉林" forKey:@"22"];
    [dic setObject:@"黑龙江" forKey:@"23"];
    [dic setObject:@"上海" forKey:@"31"];
    [dic setObject:@"江苏" forKey:@"32"];
    [dic setObject:@"浙江" forKey:@"33"];
    [dic setObject:@"安徽" forKey:@"34"];
    [dic setObject:@"福建" forKey:@"35"];
    [dic setObject:@"江西" forKey:@"36"];
    [dic setObject:@"山东" forKey:@"37"];
    [dic setObject:@"河南" forKey:@"41"];
    [dic setObject:@"湖北" forKey:@"42"];
    [dic setObject:@"湖南" forKey:@"43"];
    [dic setObject:@"广东" forKey:@"44"];
    [dic setObject:@"广西" forKey:@"45"];
    [dic setObject:@"海南" forKey:@"46"];
    [dic setObject:@"重庆" forKey:@"50"];
    [dic setObject:@"四川" forKey:@"51"];
    [dic setObject:@"贵州" forKey:@"52"];
    [dic setObject:@"云南" forKey:@"53"];
    [dic setObject:@"西藏" forKey:@"54"];
    [dic setObject:@"陕西" forKey:@"61"];
    [dic setObject:@"甘肃" forKey:@"62"];
    [dic setObject:@"青海" forKey:@"63"];
    [dic setObject:@"宁夏" forKey:@"64"];
    [dic setObject:@"新疆" forKey:@"65"];
    [dic setObject:@"台湾" forKey:@"71"];
    [dic setObject:@"香港" forKey:@"81"];
    [dic setObject:@"澳门" forKey:@"82"];
    [dic setObject:@"国外" forKey:@"91"];
    
    if ([dic objectForKey:code] == nil) {
        
        return NO;
    }
    return YES;
}



/** 银行卡号有效性问题Luhn算法
 *  现行 16 位银联卡现行卡号开头 6 位是 622126～622925 之间的，7 到 15 位是银行自定义的，
 *  可能是发卡分行，发卡网点，发卡序号，第 16 位是校验码。
 *  16 位卡号校验位采用 Luhm 校验方法计算：
 *  1，将未带校验位的 15 位卡号从右依次编号 1 到 15，位于奇数位号上的数字乘以 2
 *  2，将奇位乘积的个十位全部相加，再加上所有偶数位上的数字
 *  3，将加法和加上校验位能被 10 整除。
 */
+ (BOOL) isBankCardluhmCheck:(NSString *)bankNumber
{
    NSString * lastNum = [[bankNumber substringFromIndex:(bankNumber.length-1)] copy];//取出最后一位
    NSString * forwardNum = [[bankNumber substringToIndex:(bankNumber.length -1)] copy];//前15或18位
    
    NSMutableArray * forwardArr = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i=0; i<forwardNum.length; i++) {
        NSString * subStr = [forwardNum substringWithRange:NSMakeRange(i, 1)];
        [forwardArr addObject:subStr];
    }
    
    NSMutableArray * forwardDescArr = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = (int)(forwardArr.count-1); i> -1; i--) {//前15位或者前18位倒序存进数组
        [forwardDescArr addObject:forwardArr[i]];
    }
    
    NSMutableArray * arrOddNum = [[NSMutableArray alloc] initWithCapacity:0];//奇数位*2的积 < 9
    NSMutableArray * arrOddNum2 = [[NSMutableArray alloc] initWithCapacity:0];//奇数位*2的积 > 9
    NSMutableArray * arrEvenNum = [[NSMutableArray alloc] initWithCapacity:0];//偶数位数组
    
    for (int i=0; i< forwardDescArr.count; i++) {
        NSInteger num = [forwardDescArr[i] intValue];
        if (i%2) {//偶数位
            [arrEvenNum addObject:[NSNumber numberWithInteger:num]];
        }else{//奇数位
            if (num * 2 < 9) {
                [arrOddNum addObject:[NSNumber numberWithInteger:num * 2]];
            }else{
                NSInteger decadeNum = (num * 2) / 10;
                NSInteger unitNum = (num * 2) % 10;
                [arrOddNum2 addObject:[NSNumber numberWithInteger:unitNum]];
                [arrOddNum2 addObject:[NSNumber numberWithInteger:decadeNum]];
            }
        }
    }
    
    __block  NSInteger sumOddNumTotal = 0;
    [arrOddNum enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        sumOddNumTotal += [obj integerValue];
    }];
    
    
    __block NSInteger sumOddNum2Total = 0;
    [arrOddNum2 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        sumOddNum2Total += [obj integerValue];
    }];
    
    __block NSInteger sumEvenNumTotal = 0 ;
    [arrEvenNum enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        sumEvenNumTotal += [obj integerValue];
    }];
    
    
    NSInteger lastNumber = [lastNum integerValue];
    
    NSInteger luhmTotal = lastNumber + sumEvenNumTotal + sumOddNum2Total + sumOddNumTotal;
    
    return (luhmTotal%10 ==0)?YES:NO;
}

//获取当前年月日
+(NSString*)getCurrentYearMonthDay{
    
    NSDate *date = [NSDate date];
       
       NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
       [formatter setDateStyle:NSDateFormatterMediumStyle];
       
       [formatter setTimeStyle:NSDateFormatterShortStyle];
       
       [formatter setDateFormat:@"YYYYMMdd"];
       NSString *DateTime = [formatter stringFromDate:date];
       return DateTime;
}

+(CGFloat)getStatusBarHight {
    
        float statusBarHeight = 0;
        if (@available(iOS 13.0, *)) {
        UIStatusBarManager *statusBarManager = [UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager;
        statusBarHeight = statusBarManager.statusBarFrame.size.height;
        }
        else {
        statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        }
        return statusBarHeight;
}

@end
