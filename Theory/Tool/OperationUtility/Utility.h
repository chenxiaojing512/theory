//
//  Utility.h
//  Join
//
//  Created by 陈晓晶 on 16/11/28.
//  Copyright © 2016年 huangkejin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
typedef NS_ENUM(NSInteger,TouchIDType){
    TouchIDVerity= 0,
    TouchUnLock ,
};

/**为了不和其他命名重复，这里加上了前缀kj*/
typedef NS_ENUM(NSInteger, UtilityIphoneModels) {
    kjNone = 0,
    kjIp4,
    kjIp4s,
    kjIp5,
    kjIp5c,
    kjIp5s,
    kjIp6,
    kjIp6p,
    kjIp6s,
    kjIp6sp,
    kjIpse,
    kjIp7,
    kjIp7p,
    kjIp8,
    kjIp8p,
    kjIpx,
    kjIpxs,
    kjIpxsmax,
    kjIpxr
};


@interface Utility : NSObject

//判断是否是手机号
+ (BOOL)isValidTelephoneNum:(NSString *)strPhoneNum;

//HUD
+ (void)showProgressDialog:(UIView *)view __deprecated_msg("Use [KJDialogHUD kjShowTopActFromText: sView:]");
+ (void)showProgressDialog:(UIView *)view text:(NSString *)text __deprecated_msg("Use [KJDialogHUD kjShowTopActFromText: fullView]");
+ (void)showProgressDialogText:(NSString *)text __deprecated_msg("Use [KJDialogHUD kjShowTopActFromText:]");
+ (void)hideProgressDialog:(UIView *)view __deprecated_msg("Use [KJDialogHUD kjHideDialogFromView:]");
+ (void)hideProgressDialog __deprecated_msg("Use [KJDialogHUD kjHideDialogFromView:]");
+ (void)showCustomDialog:(UIView *)view title:(NSString *)text __deprecated_msg("Use [KJDialogHUD kjShowTopImgFromText:]");
+ (void)showAllTextDialog:(UIView *)view  Text:(NSString *)text __deprecated_msg("Use [KJDialogHUD kjShowAllText:]");


//计算一个时间与当前时间的时间差
+ (NSDateComponents *)difftimeDate:(NSDate *)date withUnit:(NSCalendarUnit)unit;
//两个时间差
+ (NSDateComponents *)date:(NSDate *)currenDate subtractDate:(NSDate *)date withUnit:(NSCalendarUnit)unit;

//获取当前控制器
+ (UIViewController *)getCurrentViewController;

//网络监测
+ (void)reachabilityStatusChange;

//字典转json字符串
+ (NSString *)dictionaryToJson:(NSDictionary *)dic;

//json字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//数组转json字符串
+ (NSString *)arrayToJsonString:(NSArray *)array;

//比较两个颜色是否相同
+ (BOOL)color:(UIColor*)firstColor equalTo:(UIColor*)secondColor;

+ (NSString *)getMMSSFromSS:(NSInteger)totalTime;

/**
 友盟分享
 
 @param kj_type 分享平台
 @param kj_url web url
 @param kj_title 标题
 @param kj_desc 副标题
 @param kj_imgUrl 图片url
 @param kj_shareType 分享类型
 @param kj_user 分享用户
 @param kj_path 微信小程序
 */
//+ (void)umShareType:(UMSocialPlatformType)kj_type
//            withUrl:(NSString *)kj_url
//              title:(NSString *)kj_title
//          descTitle:(NSString *)kj_desc
//             imgUrl:(NSString *)kj_imgUrl
//          shareType:(NSString *)kj_shareType
//         share_user:(NSString *)kj_user
//           miniPath:(NSString *)kj_path;

/**KJBaseModel解析分享数据进行分享*/
//+ (void)umShareBaseModel:(KJBaseModel *)kjModel
//              toPlatform:(UMSocialPlatformType)kj_type;

/**获取设备型号*/
+ (NSString *)getMachine;
/**获取手机型号*/
+ (UtilityIphoneModels)iphoneModels;
/**判断状态栏是否是44像素*/
+ (BOOL)if_status44;
/**判断tabbar是否是83像素*/
+ (BOOL)if_tabbar83;
/**状态栏高度*/
+ (CGFloat)kjStatusHeight;
/**Tabbar高度*/
+ (CGFloat)kjTabbarHeight;
/**导航栏高度*/
+ (CGFloat)kjNaviHeight;
/**导航栏+状态栏高度*/
+ (CGFloat)kjSNHeight;
/**导航栏+状态栏+Tabbar高度*/
+ (CGFloat)kjSNTHeight;
/**系统底部Home条的高度*/
+ (CGFloat)kjHomeIndicatorHeight;
/**判断是否有HomeIndicator*/
+ (BOOL)isHomeIndicator;
//判断字符串是否存在,如果字符串全是空格或者换行，也算不存在
+ (BOOL) isString:(NSString *)str;
//图片转字符串
+(NSString *)UIImageToBase64Str:(UIImage *) image;
+ (NSData *)imageTransFormData:(UIImage *)image;

//计算高度
+(CGFloat)calculationStringHeight:(NSDictionary*)fontDic width:(CGSize)width string:(NSString*)string;

//计算宽度
+(CGFloat)calculationStringWidth:(NSDictionary*)fontDic width:(CGSize)width string:(NSString*)string;

+(NSString*)returnMoney:(NSString*)string;

//银行卡展示格式化
+(NSString *)getNewStarBankNumWitOldNum:(NSString*)num;


//是否支持
+(BOOL)isSupport:(LAContext *)context;

/**
 验证 TouchID
 */
+(void)touchVerification:(TouchIDType)type;


//根据卡号判断银行
+ (NSString *)returnBankName:(NSString *)cardName;

//手机号
+(NSString*)returnTelPhone:(NSString*)string;

#pragma mark - 验证TouchID/FaceID
+(void)authVerification ;

+ (BOOL)JudgeTheillegalCharacter:(NSString *)content;

//获取当前时间
+(NSString*)getCurrentDate;


//获取下个月的时间
+(NSString*)getNextMonth;

+ (NSString *)getDealNumwithstring:(NSString *)string withNumCount:(NSInteger)integer;

+(BOOL)isBlankDiction:(NSDictionary *)theDict;//判断字典是否为空
+(BOOL)isBlankString:(NSString *)string;

+(BOOL)isBlankArray:(NSArray *)theArray;
+(BOOL)isBlankMuArray:(NSArray *)theArray;
+ (NSArray *)sortedRandomArrayByArray:(NSArray *)array;

//验证银行卡
/**
 身份证号校验
 
 @return YES/NO
 */
+(BOOL)yf_isIDCardNumber:(NSString*)cardNum;

//  4.身份证验证
+ (BOOL)isIdCard:(NSString *)sPaperId;
+ (BOOL)yf_validateWithRegex:(NSString *)regex ;

+ (BOOL) isBankCardluhmCheck:(NSString *)bankNumber;

//获取当前年月日
+(NSString*)getCurrentYearMonthDay;

+(CGFloat)getStatusBarHight;

@end
