//
//  KJDialogHUD.m
//  Join
//
//  Created by JOIN iOS on 2018/7/25.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import "KJDialogHUD.h"

#import "KJDialogBaseView.h"
#import "KJDialogPureText.h"
#import "KJDialogImageText.h"
#import "KJDialogActivityText.h"

@implementation KJDialogHUD

/**展示文本提示框 放置在UIWindow上*/
+ (void)kjShowAllText:(NSString *)kjText {
    KJDialogPureText *kjDialog = [[KJDialogPureText alloc] initSuperView:nil];
    kjDialog.kjDistance =  -[Utility kjSNHeight];
    kjDialog.kjBaseMode = KJDialogBaseTop;//xj 加
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:1.5];
}

/**展示文本提示框 放置在指定的UIView上*/
+ (void)kjShowAllText:(NSString *)kjText
                sView:(UIView *)kjView {
    KJDialogPureText *kjDialog = [[KJDialogPureText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:1.5];
}

/**展示文本提示框 放置在指定的UIView上，会覆盖整个View，在消失之前，该View不允许任何操作*/
+ (void)kjShowAllText:(NSString *)kjText
             fullView:(UIView *)kjView {
    KJDialogPureText *kjDialog = [[KJDialogPureText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjBaseMode = KJDialogBaseFull;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:1.5];
}

/**展示Activity文本框，Activity在左|文本在右，放置在UIWindow上*/
+ (void)kjShowLeftActFromText:(NSString *)kjText {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:nil];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                        sView:(UIView *)kjView {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，会覆盖整个View，在其消失之前，该View不允许有其它操作*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                     fullView:(UIView *)kjView {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjBaseMode = KJDialogBaseFull;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，并且整个提示框在该View的顶部展示*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                      topView:(UIView *)kjView {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = 10.0;
    kjDialog.kjBaseMode = KJDialogBaseTop;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，并且整个提示框在该View的顶部展示,并制定顶部距离*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                      topView:(UIView *)kjView
                  topDistance:(CGFloat)distance {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = distance;
    kjDialog.kjBaseMode = KJDialogBaseTop;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在上|文本在下，放置在UIWindow上*/
+ (void)kjShowTopActFromText:(NSString *)kjText {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:nil];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjDirection = KJDialogActivityTop;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在上|文本在下，放置在指定的UIView上*/
+ (void)kjShowTopActFromText:(NSString *)kjText
                       sView:(UIView *)kjView {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjDirection = KJDialogActivityTop;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示Activity文本框，Activity在上|文本在下，放置在指定的UIView上，会覆盖整个View，让View不能有其它任何操作*/
+ (void)kjShowTopActFromText:(NSString *)kjText
                    fullView:(UIView *)kjView {
    KJDialogActivityText *kjDialog = [[KJDialogActivityText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjBaseMode = KJDialogBaseFull;
    kjDialog.kjDirection = KJDialogActivityTop;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjLabel.text = kjText;
    [kjDialog kjShowTime:0];
}

/**展示图片文本框，图片在上|文本在下，放置在UIWindow上*/
+ (void)kjShowTopImgFromText:(NSString *)kjText {
    KJDialogImageText *kjDialog = [[KJDialogImageText alloc] initSuperView:nil];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjDirection = KJDialogImageTop;
    kjDialog.kjLabel.text = kjText;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    UIImage *image = [UIImage imageNamed:@"save_complete_icon"];
    kjDialog.kjImgV.image = image;
    kjDialog.kjSize = image.size;
    [kjDialog kjShowTime:1.5];
}

/**展示图片文本框，图片在上|文本在下，放置在指定的UIView上*/
+ (void)kjShowTopImgFromText:(NSString *)kjText
                       sView:(UIView *)kjView {
    KJDialogImageText *kjDialog = [[KJDialogImageText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjDirection = KJDialogImageTop;
    kjDialog.kjLabel.text = kjText;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjImgV.image = [UIImage imageNamed:@"save_complete_icon"];
    [kjDialog kjShowTime:1.5];
}

/**展示图片文本框，图片在上|文本在下，放置在指定的UIView上，并覆盖整个UIView*/
+ (void)kjShowTopImgFromText:(NSString *)kjText
                    fullView:(UIView *)kjView {
    KJDialogImageText *kjDialog = [[KJDialogImageText alloc] initSuperView:kjView];
    kjDialog.kjDistance = -49.0;
    kjDialog.kjDirection = KJDialogImageTop;
    kjDialog.kjLabel.text = kjText;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjImgV.image = [UIImage imageNamed:@"save_complete_icon"];
    [kjDialog kjShowTime:1.5];
}


/**
 展示gif

 @param images 图片组<UIImage *>
 @param kjText 文案 可有可无
 @param kjView 展示的地方
 */
+ (void)kjShowAnimationImages:(NSArray <UIImage *>*)images
                         text:(NSString *)kjText
                        sView:(UIView *)kjView {
    KJDialogImageText *kjDialog = [[KJDialogImageText alloc] initSuperView:kjView];
    kjDialog.kjBaseColor = [UIColor clearColor];
    kjDialog.kjBaseMode = KJDialogBaseTop;
    kjDialog.kjDistance = [Utility kjSNHeight];
    kjDialog.kjDirection = KJDialogImageBottom;
    kjDialog.kjLabel.text = kjText;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjImgV.animationImages = images;
    kjDialog.kjImgV.animationDuration = images.count * 0.1;
    UIImage *image = images.firstObject;
    kjDialog.kjSize = image.size;
    kjDialog.kjMinHeight = 60;
    [kjDialog.kjImgV startAnimating];
    [kjDialog kjShowTime:0];
}

/**
 展示gif
 
 @param images 图片组<UIImage *>
 @param kjText 文案 可有可无
 @param kjView 展示的地方
 */
+ (void)kjShowCenterAniImages:(NSArray <UIImage *>*)images
                         text:(NSString *)kjText
                        sView:(UIView *)kjView {
    KJDialogImageText *kjDialog = [[KJDialogImageText alloc] initSuperView:kjView];
    kjDialog.kjBaseColor = [UIColor clearColor];
    kjDialog.kjDistance = -[Utility kjSNHeight];
    kjDialog.kjDirection = KJDialogImageBottom;
    kjDialog.kjLabel.text = kjText;
    kjDialog.kjLabel.font = [UIFont systemFontOfSize:15.0];
    kjDialog.kjImgV.animationImages = images;
    kjDialog.kjImgV.animationDuration = images.count * 0.1;
    UIImage *image = images.firstObject;
    kjDialog.kjSize = image.size;
    kjDialog.kjMinHeight = 60;
    [kjDialog.kjImgV startAnimating];
    [kjDialog kjShowTime:0];
}

/**让指定的UIView上的所有提示框消失*/
+ (void)kjHideDialogFromView:(UIView *)kjView {
    [KJDialogBaseView kjHiddenFromSuperView:kjView];
}

@end
