//
//  KJDialogHUD.h
//  Join
//
//  Created by JOIN iOS on 2018/7/25.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KJDialogHUD : NSObject

/**展示文本提示框 放置在UIWindow上*/
+ (void)kjShowAllText:(NSString *)kjText;

/**展示文本提示框 放置在指定的UIView上*/
+ (void)kjShowAllText:(NSString *)kjText
                sView:(UIView *)kjView;

/**展示文本提示框 放置在指定的UIView上，会覆盖整个View，在消失之前，该View不允许任何操作*/
+ (void)kjShowAllText:(NSString *)kjText
             fullView:(UIView *)kjView;

/**展示Activity文本框，Activity在左|文本在右，放置在UIWindow上*/
+ (void)kjShowLeftActFromText:(NSString *)kjText;

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                        sView:(UIView *)kjView;

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，会覆盖整个View，在其消失之前，该View不允许有其它操作*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                     fullView:(UIView *)kjView;

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，并且整个提示框在该View的顶部展示*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                      topView:(UIView *)kjView;

/**展示Activity文本框，Activity在左|文本在右，放置在指定的UIView上，并且整个提示框在该View的顶部展示,并制定顶部距离*/
+ (void)kjShowLeftActFromText:(NSString *)kjText
                      topView:(UIView *)kjView
                  topDistance:(CGFloat)distance;

/**展示Activity文本框，Activity在上|文本在下，放置在UIWindow上*/
+ (void)kjShowTopActFromText:(NSString *)kjText;

/**展示Activity文本框，Activity在上|文本在下，放置在指定的UIView上*/
+ (void)kjShowTopActFromText:(NSString *)kjText
                       sView:(UIView *)kjView;

/**展示Activity文本框，Activity在上|文本在下，放置在指定的UIView上，会覆盖整个View，让View不能有其它任何操作*/
+ (void)kjShowTopActFromText:(NSString *)kjText
                    fullView:(UIView *)kjView;

/**展示图片文本框，图片在上|文本在下，放置在UIWindow上*/
+ (void)kjShowTopImgFromText:(NSString *)kjText;

/**展示图片文本框，图片在上|文本在下，放置在指定的UIView上*/
+ (void)kjShowTopImgFromText:(NSString *)kjText
                       sView:(UIView *)kjView;

/**展示图片文本框，图片在上|文本在下，放置在指定的UIView上，并覆盖整个UIView*/
+ (void)kjShowTopImgFromText:(NSString *)kjText
                    fullView:(UIView *)kjView;

/**
 展示gif加载样式
 
 @param images 图片组<UIImage *>
 @param kjText 文案 可有可无
 @param kjView 展示的地方
 */
+ (void)kjShowAnimationImages:(NSArray <UIImage *>*)images
                         text:(NSString *)kjText
                        sView:(UIView *)kjView;

/**
 展示gif
 
 @param images 图片组<UIImage *>
 @param kjText 文案 可有可无
 @param kjView 展示的地方
 */
+ (void)kjShowCenterAniImages:(NSArray <UIImage *>*)images
                         text:(NSString *)kjText
                        sView:(UIView *)kjView;


/**让指定的UIView上的所有提示框消失*/
+ (void)kjHideDialogFromView:(UIView *)kjView;

@end
