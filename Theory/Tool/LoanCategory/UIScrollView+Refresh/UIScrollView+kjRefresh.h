//
//  UIScrollView+kjRefresh.h
//  JoinManager
//
//  Created by JOIN iOS on 2018/3/13.
//  Copyright © 2018年 kegem@foxmail.com（Kegem）. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+kjRefreshExtension.h"

@interface UIScrollView (kjRefresh)

/**刷新数据展示reloadData   noMoreData-YES表示没有更多数据了*/
- (void)kj_reloadData:(BOOL)noMoreData;
/**停止刷新，但不刷新UITableView/UICollectionView*/
- (void)kjEndRefresh:(BOOL)noMoreData;
/**这个会让刷新控件显示*/
- (void)beginRefresh;
/**这个不会显示刷新控件*/
- (void)refresh;

@end
