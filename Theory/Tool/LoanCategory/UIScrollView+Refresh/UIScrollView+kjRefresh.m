//
//  UIScrollView+kjRefresh.m
//  JoinManager
//
//  Created by JOIN iOS on 2018/3/13.
//  Copyright © 2018年 kegem@foxmail.com（Kegem）. All rights reserved.
//

#import "UIScrollView+kjRefresh.h"
#import <MJRefresh/MJRefresh.h>
#include <pthread.h>
@implementation UIScrollView (kjRefresh)

static char *kjHeaderKey  = "kjHiddenHeader";
static char *kjFooterKey  = "kjHiddenFooter";
static char *kjDelegateKey  = "kjRefreshDeleagte";
static char *kjHiddenRefreshKey  = "kjHiddenRefresh";
static char *kjRefreshBlockKey  = "kjRefreshBlockKey";
static char *kjLoadMoreBlockKey  = "kjLoadMoreBlockKey";
static char *kjTriggerBlockKey = "kjTriggerBlockKey";
static char *kjTriggerAnimationKey = "kjTriggerAnimationKey";

/**创建header*/
- (void)kj_header {
    if (!self.mj_header) {
        //由于适配JOIN做下面更改,如果其它应用使用的话需要打开下面的注释，删除JOIN部分
        
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self
                                                                   refreshingAction:@selector(kj_refresh)];
        // 设置自动切换透明度(在导航栏下面自动隐藏)
        header.automaticallyChangeAlpha = YES;
        //设置动画  自定义下拉刷新动画
        /*
        if (self.kjHeaderImages.count > 0) {
            [header setImages:self.kjHeaderImages forState:MJRefreshStatePulling];
            [header setImages:self.kjHeaderImages forState:MJRefreshStateRefreshing];
            [header setImages:self.kjHeaderImages forState:MJRefreshStateWillRefresh];
        } else {
//            [header setImages:[UserInfo currentUser].refreshBlackImages forState:MJRefreshStatePulling];
//            [header setImages:[UserInfo currentUser].refreshBlackImages forState:MJRefreshStateRefreshing];
//            [header setImages:[UserInfo currentUser].refreshBlackImages forState:MJRefreshStateWillRefresh];
        }
        
        // 隐藏时间
        header.lastUpdatedTimeLabel.hidden = YES;
        // 隐藏状态
        header.stateLabel.hidden = YES;
        self.mj_header = header;*/
        
        
        
        if (self.kjHeaderImages.count > 0) {
            MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self
                                                                       refreshingAction:@selector(kj_refresh)];
            // 设置自动切换透明度(在导航栏下面自动隐藏)
            header.automaticallyChangeAlpha = YES;
            //设置动画
            [header setImages:self.kjHeaderImages forState:MJRefreshStatePulling];
            [header setImages:self.kjHeaderImages forState:MJRefreshStateRefreshing];
            [header setImages:self.kjHeaderImages forState:MJRefreshStateWillRefresh];
            // 隐藏时间
            header.lastUpdatedTimeLabel.hidden = YES;
            // 隐藏状态
            header.stateLabel.hidden = YES;
            self.mj_header = header;
        } else {
            MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self
                                                                             refreshingAction:@selector(kj_refresh)];
            // 设置自动切换透明度(在导航栏下面自动隐藏)
            header.automaticallyChangeAlpha = YES;
            // 隐藏时间
            header.lastUpdatedTimeLabel.hidden = YES;
            
            self.mj_header = header;
        }
        
    }
}

/**创建footer*/
- (void)kj_footer {
    if (!self.mj_footer) {
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(kj_loadMore)];
        footer.activityIndicatorViewStyle = UIActivityIndicatorViewStyleMedium;
//        footer.onlyRefreshPerDrag = YES;
        if (self.kjHeaderImages.count > 0) {
            footer.stateLabel.textColor = [UIColor whiteColor];
        }
        // 设置footer
        self.mj_footer = footer;
    }
}

/**设置动画图片组*/
- (void)setKjHeaderImages:(NSArray *)kjHeaderImages {
    objc_setAssociatedObject(self, &kjTriggerAnimationKey, kjHeaderImages, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)kjHeaderImages {
    return objc_getAssociatedObject(self, &kjTriggerAnimationKey);
}

/**设置代理*/
- (void)setKjRefresh:(id<KJRefreshDelegate>)kjRefresh {
    [self kj_header];
    objc_setAssociatedObject(self, &kjDelegateKey, kjRefresh, OBJC_ASSOCIATION_ASSIGN);
}

- (id<KJRefreshDelegate>)kjRefresh {
    return objc_getAssociatedObject(self, &kjDelegateKey);
}

/**block*/
- (void)setKjTriggerRefresh:(void (^)())kjTriggerRefresh {
    [self kj_header];
    objc_setAssociatedObject(self, &kjRefreshBlockKey, kjTriggerRefresh, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void(^)())kjTriggerRefresh {
    return objc_getAssociatedObject(self, &kjRefreshBlockKey);
}

- (void)setKjTriggerLoadMore:(void (^)())kjTriggerLoadMore {
    objc_setAssociatedObject(self, &kjLoadMoreBlockKey, kjTriggerLoadMore, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void(^)())kjTriggerLoadMore {
    return objc_getAssociatedObject(self, &kjLoadMoreBlockKey);
}

- (void)setKjTriggerBlock:(void (^)(KJRefreshTriggerType))kjTriggerBlock {
    [self kj_header];
    objc_setAssociatedObject(self, &kjTriggerBlockKey, kjTriggerBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void(^)(KJRefreshTriggerType kjType))kjTriggerBlock {
    return objc_getAssociatedObject(self, &kjTriggerBlockKey);
}

/**删除header 即没有下拉刷新*/
- (void)setKjHiddenHeader:(BOOL)kjHiddenHeader {
    if (kjHiddenHeader) {
        [self.mj_header endRefreshing];
        self.mj_header = nil;
    } else {
        [self kj_header];
    }
    objc_setAssociatedObject(self, &kjHeaderKey, [NSString stringWithFormat:@"%d",kjHiddenHeader], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)kjHiddenHeader {
    return [objc_getAssociatedObject(self, &kjHeaderKey) boolValue];
}

/**删除footer 即没有上拉加载*/
- (void)setKjHiddenFooter:(BOOL)kjHiddenFooter {
    if (kjHiddenFooter) {
        [self.mj_footer endRefreshing];
        self.mj_footer = nil;
    } else {
        [self kj_footer];
    }
    objc_setAssociatedObject(self, &kjFooterKey, [NSString stringWithFormat:@"%d",kjHiddenFooter], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)kjHiddenFooter {
    return [objc_getAssociatedObject(self, &kjFooterKey) boolValue];
}

/**禁用下拉刷新和上拉加载功能*/
- (void)setKjHiddenRefresh:(BOOL)kjHiddenRefresh {
    self.kjHiddenHeader = kjHiddenRefresh;
    self.kjHiddenFooter = kjHiddenRefresh;
    
    objc_setAssociatedObject(self, &kjHiddenRefreshKey, [NSString stringWithFormat:@"%d",kjHiddenRefresh], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)kjHiddenRefresh {
    return [objc_getAssociatedObject(self, &kjHiddenRefreshKey) boolValue];
}

/**下拉刷新*/
- (void)kj_refresh {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.kjRefresh && [self.kjRefresh respondsToSelector:@selector(kj_didTriggerRefresh)]) {
            [self.kjRefresh kj_didTriggerRefresh];
        }
        if (self.kjRefresh && [self.kjRefresh respondsToSelector:@selector(kj_didTriggerType:)]) {
            [self.kjRefresh kj_didTriggerType:KJTriggerRefresh];
        }
        if (self.kjTriggerRefresh) {
            self.kjTriggerRefresh();
        }
        if (self.kjTriggerBlock) {
            self.kjTriggerBlock(KJTriggerRefresh);
        }
    });
}

/**上拉加载*/
- (void)kj_loadMore {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.kjRefresh && [self.kjRefresh respondsToSelector:@selector(kj_didTriggerLoadMore)]) {
            [self.kjRefresh kj_didTriggerLoadMore];
        }
        if (self.kjRefresh && [self.kjRefresh respondsToSelector:@selector(kj_didTriggerType:)]) {
            [self.kjRefresh kj_didTriggerType:KJTriggerLoadMore];
        }
        if (self.kjTriggerLoadMore) {
            self.kjTriggerLoadMore();
        }
        if (self.kjTriggerBlock) {
            self.kjTriggerBlock(KJTriggerLoadMore);
        }
    });
}

- (void)refresh {
    [self kj_refresh];
}

- (void)beginRefresh {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.kjHiddenHeader) {
            [self kj_refresh];
        } else {
            [self.mj_header beginRefreshing];
        }
    });
}


/**刷新数据展示reloadData*/
- (void)kj_reloadData:(BOOL)noMoreData {
    if (pthread_main_np()) {
        [self kjReloadData];
    } else {
        [self performSelectorOnMainThread:@selector(kjReloadData) withObject:nil waitUntilDone:NO];
    }
    [self kjEndRefresh:noMoreData];
}

- (void)kjReloadData {
    if ([self isKindOfClass:[UITableView class]]) {
        [UIView performWithoutAnimation:^{
            [(UITableView *)self reloadData];
        }];
    } else if ([self isKindOfClass:[UICollectionView class]]) {
        [UIView performWithoutAnimation:^{
            [(UICollectionView *)self reloadData];
        }];
    }
}

/**停止刷新，但不刷新UITableView/UICollectionView*/
- (void)kjEndRefresh:(BOOL)noMoreData {
    if (pthread_main_np()) {
        [self stopRefresh:@(noMoreData)];
    } else {
        [self performSelectorOnMainThread:@selector(stopRefresh:) withObject:@(noMoreData) waitUntilDone:NO];
    }
}

- (void)stopRefresh:(NSNumber *)noMoreData {
    if (self.mj_header) {
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.mj_header endRefreshing];
    }
    
    if (self.mj_footer) {
        // 拿到当前的上拉刷新控件，结束刷新状态
        [self.mj_footer endRefreshing];
    }
    
    if (noMoreData.boolValue) {
        if (self.mj_footer) {
            // 拿到当前的上拉刷新控件，变为没有更多数据的状态
            [self.mj_footer endRefreshingWithNoMoreData];
        }
    } else {
        if (self.mj_footer) {
            // 拿到当前的上拉刷新控件，恢复数据加载
            [self.mj_footer resetNoMoreData];
        } else {
            if (!self.kjHiddenFooter) {
                [self kj_footer];
            }
        }
    }
    if (self.mj_footer) {
        self.mj_footer.hidden = noMoreData.boolValue;
    }
    
}

@end
