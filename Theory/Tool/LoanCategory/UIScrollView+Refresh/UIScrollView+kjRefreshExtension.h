//
//  UIScrollView+kjRefreshExtension.h
//  JoinManager
//
//  Created by JOIN iOS on 2018/3/13.
//  Copyright © 2018年 kegem@foxmail.com（Kegem）. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, KJRefreshTriggerType) {
    KJTriggerRefresh = 0,
    KJTriggerLoadMore
};

@protocol KJRefreshDelegate <NSObject>

@optional

/**刷新*/
- (void)kj_didTriggerRefresh;
/**加载*/
- (void)kj_didTriggerLoadMore;
/**刷新、加载*/
- (void)kj_didTriggerType:(KJRefreshTriggerType)kjType;

@end

@interface UIScrollView ()
/**如需要动画刷新效果，设置动画图片组,如果需要，请在设置代理或者block前设置*/
@property (strong, nonatomic) NSArray *kjHeaderImages;
/**删除header 即没有(禁用)下拉刷新*/
@property (assign, nonatomic) BOOL kjHiddenHeader;
/**删除footer 即没有(禁用)上拉加载*/
@property (assign, nonatomic) BOOL kjHiddenFooter;
/**禁用下拉刷新和上拉加载功能 即同时删除header和footer*/
@property (assign, nonatomic) BOOL kjHiddenRefresh;
/**代理*/
@property (weak, nonatomic, nullable) id <KJRefreshDelegate> kjRefresh;
/**block和代理二选一即可*/
@property (copy, nonatomic) void (^ kjTriggerRefresh)();
@property (copy, nonatomic) void (^ kjTriggerLoadMore)();
/**上面两个block是刷新和加载分开，下面这个就是合并到一起*/
@property (copy, nonatomic) void (^ kjTriggerBlock)(KJRefreshTriggerType kjType);

@end
