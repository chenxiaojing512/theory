//
//  UILabel+DKLabel.h
//  Loan
//
//  Created by 陈晓晶 on 2019/8/20.
//  Copyright © 2019 陈晓晶. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN
/* Values for NSWritingDirection */
typedef NS_ENUM(NSInteger, MCTextDirection) {
    MCTextDirectionNatural       = -1,    // Determines direction using the Unicode Bidi Algorithm rules P2 and P3
    MCTextDirectionLeft   =  0,    // Left to right writing direction
    MCTextDirectionRight   =  1     // Right to left writing direction
};
@interface UILabel (DKLabel)
/**
 *  设置字间距
 */
- (void)setColumnSpace:(CGFloat)columnSpace;
/**
 *  设置行距
 */
- (void)setRowSpace:(CGFloat)rowSpace;
/**
 *  设置文字对齐方向
 */
- (void)setTextDirection:(MCTextDirection)direction;

@end

NS_ASSUME_NONNULL_END
