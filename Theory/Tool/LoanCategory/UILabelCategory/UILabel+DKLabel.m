//
//  UILabel+DKLabel.m
//  Loan
//
//  Created by 陈晓晶 on 2019/8/20.
//  Copyright © 2019 陈晓晶. All rights reserved.
//

#import "UILabel+DKLabel.h"
#import <CoreText/CoreText.h>

@implementation UILabel (DKLabel)

- (void)setColumnSpace:(CGFloat)columnSpace
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    //调整间距
    [attributedString addAttribute:(__bridge NSString *)kCTKernAttributeName value:@(columnSpace) range:NSMakeRange(0, [attributedString length])];
    self.attributedText = attributedString;
}

- (void)setRowSpace:(CGFloat)rowSpace
{
    self.numberOfLines = 0;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    //调整行距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = rowSpace;
    //    paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
    self.attributedText = attributedString;
}

- (void)setTextDirection:(MCTextDirection)direction
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    //调整行距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    if (direction == MCTextDirectionNatural) {
        paragraphStyle.baseWritingDirection = NSWritingDirectionNatural;
    }else if (direction == MCTextDirectionRight){
        paragraphStyle.baseWritingDirection = NSWritingDirectionRightToLeft;
    }else{
        paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;
    }
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
    self.attributedText = attributedString;
}
@end
