//
//  NSAttributedString+size.h
//  KJNetworkRequest
//
//  Created by 黄克瑾 on 2020/6/19.
//  Copyright © 2020 黄克瑾. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSAttributedString (size)

/// 根据最大宽度计算size
/// @param width 最大宽度
- (CGSize)sizeWithMaxWidth:(CGFloat)width;

/// 计算文本的高度
/// @param width 最大宽度
- (CGFloat)heightWithMaxWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
