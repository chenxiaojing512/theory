//
//  NSAttributedString+size.m
//  KJNetworkRequest
//
//  Created by 黄克瑾 on 2020/6/19.
//  Copyright © 2020 黄克瑾. All rights reserved.
//

#import "NSAttributedString+size.h"

@implementation NSAttributedString (size)

- (CGSize)sizeWithMaxWidth:(CGFloat)width {
    return [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                              options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                              context:nil].size;
}

/// 计算文本的高度
/// @param width 最大宽度
- (CGFloat)heightWithMaxWidth:(CGFloat)width {
    return ceilf([self sizeWithMaxWidth:width].height);
}

@end
