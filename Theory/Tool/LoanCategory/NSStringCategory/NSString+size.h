//
//  NSString+size.h
//  KJNetworkRequest
//
//  Created by 黄克瑾 on 2020/6/19.
//  Copyright © 2020 黄克瑾. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (size)

/// 计算文本的宽高
/// @param font 文本字体
/// @param width 最大宽度
- (CGSize)sizeWithFont:(UIFont *)font
              maxWidth:(CGFloat)width;

/// 计算文本的高度
/// @param font 字体
/// @param width 最大宽度
- (CGFloat)heightWithFont:(UIFont *)font
                 maxWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
