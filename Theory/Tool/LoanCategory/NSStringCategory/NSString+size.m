//
//  NSString+size.m
//  KJNetworkRequest
//
//  Created by 黄克瑾 on 2020/6/19.
//  Copyright © 2020 黄克瑾. All rights reserved.
//

#import "NSString+size.h"
#import "NSAttributedString+size.h"

@implementation NSString (size)

/// 计算文本的宽高
/// @param font 文本字体
/// @param width 最大宽度
- (CGSize)sizeWithFont:(UIFont *)font
              maxWidth:(CGFloat)width {
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.maximumLineHeight = font.lineHeight;
    style.minimumLineHeight = font.lineHeight;
    return [[[NSAttributedString alloc] initWithString:self attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: style}] sizeWithMaxWidth:width];
}

/// 计算文本的高度
/// @param font 字体
/// @param width 最大宽度
- (CGFloat)heightWithFont:(UIFont *)font
                 maxWidth:(CGFloat)width {
    return ceilf([self sizeWithFont:font maxWidth:width].height);
}

@end
