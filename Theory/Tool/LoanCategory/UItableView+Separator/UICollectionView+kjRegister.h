//
//  UICollectionView+kjRegister.h
//  Join
//
//  Created by JOIN iOS on 2018/5/21.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (kjRegister)

/**
 xib - 注册UICollectionViewCell identifier默认为nibName
 
 @param kjName nibName
 */
- (void)kjRegisXibName:(NSString *)kjName;

/**
 xib - 注册UICollectionViewCell 指定identifier
 
 @param kjName nibName
 @param kjIdentifier identifier
 */
- (void)kjRegisXibName:(NSString *)kjName identifier:(NSString *)kjIdentifier;

/**
 纯代码 - 注册UICollectionViewCell identifier默认为class.name
 
 @param kjCell UICollectionViewCell.class
 */
- (void)kjRegisCell:(Class)kjCell;

/**
 纯代码 - 注册UICollectionViewCell 指定identifier
 
 @param kjCell UICollectionViewCell.class
 @param kjIdentifier identifier
 */
- (void)kjRegisCell:(Class)kjCell identifier:(NSString *)kjIdentifier;

/**
 获取展示 identifier默认为class.name
 
 @param kjCell UICollectionViewCell.class
 @param kjIndexPath NSIndexPath
 @return UICollectionViewCell
 */
- (id)kjDequeueCell:(Class)kjCell index:(NSIndexPath *)kjIndexPath;

/**
 获取展示 指定identifier
 
 @param kjIdentifier identifier
 @param kjIndexPath NSIndexPath
 @return UICollectionViewCell
 */
- (id)kjDequeueCellIdentifier:(NSString *)kjIdentifier index:(NSIndexPath *)kjIndexPath;

@end
