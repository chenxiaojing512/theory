//
//  UITableView+Separator.h
//  Join
//
//  Created by 陈晓晶 on 2017/1/13.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Separator)

#pragma mark - xib创建的UITableViewCell
/**xib-注册UITableViewCell   identifier默认为nibName*/
- (void)kjRegisXibName:(NSString *)kjName;
/**
 xib-注册UITableViewCell，并指定identifier
 
 @param kjName nibName
 @param kjIdentifier identifier
 */
- (void)kjRegisXibName:(NSString *)kjName
            identifier:(NSString *)kjIdentifier;


#pragma mark - 代码创建的UITableViewCell
/** 代码-注册UITableViewCell*/
- (void)kjRegisCell:(Class)kjCell;

/**
 代码-注册UITableViewCell，并指定identifier
 
 @param kjCell UITableViewCell.class
 @param kjIdentifier identifier
 */
- (void)kjRegisCell:(Class)kjCell
         identifier:(NSString *)kjIdentifier;


#pragma mark - 展示
/**获取展示 默认cell.name*/
- (id)kjDequeueCell:(Class)kjCell;

/**获取展示 指定identifier*/
- (id)kjDequeueCellIdentifier:(NSString *)kjIdentifier;

/**获取展示 指定NSIndexPath*/
- (id)kjDequeueCell:(Class)kjCell index:(NSIndexPath *)kjIndex;

/**获取展示 指定identifier和NSIndexPath*/
- (id)kjDequeueCellidentifier:(NSString *)kjIdentifier index:(NSIndexPath *)kjIndex;

@end
