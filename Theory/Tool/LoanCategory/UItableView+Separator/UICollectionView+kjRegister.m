//
//  UICollectionView+kjRegister.m
//  Join
//
//  Created by JOIN iOS on 2018/5/21.
//  Copyright © 2018年 huangkejin. All rights reserved.
//

#import "UICollectionView+kjRegister.h"

@implementation UICollectionView (kjRegister)


/**
 xib - 注册UICollectionViewCell identifier默认为nibName

 @param kjName nibName
 */
- (void)kjRegisXibName:(NSString *)kjName {
    [self registerNib:[UINib nibWithNibName:kjName
                                     bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kjName];
}


/**
 xib - 注册UICollectionViewCell 指定identifier

 @param kjName nibName
 @param kjIdentifier identifier
 */
- (void)kjRegisXibName:(NSString *)kjName identifier:(NSString *)kjIdentifier {
    [self registerNib:[UINib nibWithNibName:kjName
                                     bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kjIdentifier];
}



/**
 纯代码 - 注册UICollectionViewCell identifier默认为class.name

 @param kjCell UICollectionViewCell.class
 */
- (void)kjRegisCell:(Class)kjCell {
    [self registerClass:kjCell forCellWithReuseIdentifier:NSStringFromClass(kjCell)];
}


/**
 纯代码 - 注册UICollectionViewCell 指定identifier

 @param kjCell UICollectionViewCell.class
 @param kjIdentifier identifier
 */
- (void)kjRegisCell:(Class)kjCell identifier:(NSString *)kjIdentifier {
    [self registerClass:kjCell forCellWithReuseIdentifier:kjIdentifier];
}




/**
 获取展示 identifier默认为class.name

 @param kjCell UICollectionViewCell.class
 @param kjIndexPath NSIndexPath
 @return UICollectionViewCell
 */
- (id)kjDequeueCell:(Class)kjCell index:(NSIndexPath *)kjIndexPath {
    return [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass(kjCell) forIndexPath:kjIndexPath];
}


/**
 获取展示 指定identifier

 @param kjIdentifier identifier
 @param kjIndexPath NSIndexPath
 @return UICollectionViewCell
 */
- (id)kjDequeueCellIdentifier:(NSString *)kjIdentifier index:(NSIndexPath *)kjIndexPath {
    return [self dequeueReusableCellWithReuseIdentifier:kjIdentifier forIndexPath:kjIndexPath];
}

@end
