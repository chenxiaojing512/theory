//
//  UITableView+Separator.m
//  Join
//
//  Created by 陈晓晶 on 2017/1/13.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import "UITableView+Separator.h"
#import "NSObject+SXRuntime.h"

@implementation UITableView (Separator)

/*
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleInstanceMethodWithOriginSel:@selector(layoutSubviews) swizzledSel:@selector(kj_layoutSubviews)];
    });
}

- (void)kj_layoutSubviews {
    [self kj_layoutSubviews];
    [self setSeparatorColor:[UIColor colorWithHex:0xe1e1e1]];
    if (self.tag < 999 && ![NSStringFromClass([self class]) isEqualToString:@"UIPickerTableView"]) {
        self.backgroundColor = [UIColor colorWithHex:tableViewColor];
    }
}

*/

#pragma mark - xib创建的UITableViewCell
/**xib-注册UITableViewCell   identifier默认为nibName*/
- (void)kjRegisXibName:(NSString *)kjName {
    [self registerNib:[UINib nibWithNibName:kjName
                                     bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kjName];
}

/**
 xib-注册UITableViewCell，并指定identifier
 
 @param kjName nibName
 @param kjIdentifier identifier
 */
- (void)kjRegisXibName:(NSString *)kjName
            identifier:(NSString *)kjIdentifier {
    [self registerNib:[UINib nibWithNibName:kjName
                                     bundle:[NSBundle mainBundle]]forCellReuseIdentifier:kjIdentifier];
}


#pragma mark - 代码创建的UITableViewCell
/** 代码-注册UITableViewCell*/
- (void)kjRegisCell:(Class)kjCell {
    [self registerClass:kjCell forCellReuseIdentifier:NSStringFromClass(kjCell)];
}

/**
 代码-注册UITableViewCell，并指定identifier
 
 @param kjCell UITableViewCell.class
 @param kjIdentifier identifier
 */
- (void)kjRegisCell:(Class)kjCell
         identifier:(NSString *)kjIdentifier {
    [self registerClass:kjCell forCellReuseIdentifier:kjIdentifier];
}



#pragma mark - 展示
/**获取展示 默认cell.name*/
- (id)kjDequeueCell:(Class)kjCell {
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass(kjCell)];
}

/**获取展示 指定identifier*/
- (id)kjDequeueCellIdentifier:(NSString *)kjIdentifier {
    return [self dequeueReusableCellWithIdentifier:kjIdentifier];
}

/**获取展示 指定NSIndexPath*/
- (id)kjDequeueCell:(Class)kjCell index:(NSIndexPath *)kjIndex {
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass(kjCell) forIndexPath:kjIndex];
}

/**获取展示 指定identifier和NSIndexPath*/
- (id)kjDequeueCellidentifier:(NSString *)kjIdentifier index:(NSIndexPath *)kjIndex {
    return [self dequeueReusableCellWithIdentifier:kjIdentifier forIndexPath:kjIndex];
}

@end
