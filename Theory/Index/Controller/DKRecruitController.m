//
//  DKRecruitController.m
//  Theory
//
//  Created by xiaojing on 2020/6/24.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKRecruitController.h"
#import "DKRecruitCell.h"

@interface DKRecruitController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tView;
@end

@implementation DKRecruitController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

-(void)setupTableView{
    
    self.tView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tView.delegate = self;
    self.tView.dataSource = self;
    self.tView.backgroundColor = UIColorFromRGB(0xF7F7F7);
    [self.tView kjRegisXibName:@"DKRecruitCell"];
    self.tView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tView];
    [self.tView mas_makeConstraints:^(MASConstraintMaker *make) {
         
        make.edges.equalTo(self.view);
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 100;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 150;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DKRecruitCell *cell = [tableView kjDequeueCell:[DKRecruitCell class] index:indexPath];
    return cell;

}



@end
