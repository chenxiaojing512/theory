//
//  DKIndexController.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKIndexController.h"
#import "DKAcademicController.h"//学术圈
#import "DKReCommendController.h"//推荐
#import "DKHotArticleController.h"//热门评论
#import "DKSearchView.h"//导航搜索view
#import "DKDefaultSearchController.h" //默认搜索页面
#import "DKRecruitController.h" //招聘

@interface DKIndexController ()<SGPageTitleViewDelegate,SGPageContentCollectionViewDelegate>
@property (nonatomic,strong)NSMutableArray *titleArray;
@property (nonatomic,strong)NSMutableArray *childVCArray;
//标题view
@property (nonatomic,strong)SGPageTitleView *pageTitleView;
//内容view
@property (nonatomic,strong)SGPageContentCollectionView *pageContentCollectionView;
@end

@implementation DKIndexController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.fd_prefersNavigationBarHidden = YES;

    
    DKSearchView *nav = [[DKSearchView alloc]initWithFrame:CGRectMake(0, KStatusBarHeight, kScreenWidth, KNavBarHeight)];
    [nav setSearchTapBlock:^{
        DKDefaultSearchController *vc = [[DKDefaultSearchController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    nav.intrinsicContentSize = CGSizeMake(kScreenWidth, KNavBarHeight);

    self.navigationItem.titleView = nav;
    
    self.titleArray  = [[NSMutableArray alloc]init];
    self.childVCArray = [[NSMutableArray alloc]init];
    NSArray *titleArr = @[@{@"sid":@"0",@"name":@"学术圈"},
                          @{@"sid":@"1",@"name":@"推荐"},
                          @{@"sid":@"2",@"name":@"热门论文"},
                          @{@"sid":@"3",@"name":@"视频"},
                          @{@"sid":@"3",@"name":@"招聘"},
    ];
    
    [self setupPageView:titleArr];
    


}

-(void)setupPageView:(NSArray*)array{
    
    
    for (NSInteger i = 0; i < array.count; i++) {
        
        [self.titleArray addObject:array[i][@"name"]];
    }
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.bottomSeparatorColor = UIColorFromRGB(0xF7F7F7);
    configure.showIndicator = YES;
    configure.indicatorColor = UIColorFromRGB(0x1E49AF);
    configure.indicatorHeight = 2;
    configure.titleColor = UIColorFromRGB(0x5F7192);
    configure.titleSelectedColor = UIColorFromRGB(0x000000);
    configure.titleFont = [UIFont systemFontOfSize:13];
    configure.titleSelectedFont = [UIFont systemFontOfSize:18];
//    configure.titleAdditionalWidth = 10;
    configure.indicatorAdditionalWidth = -10;
    
    CGFloat pageTitleY = KStatusBarHeight + KNavBarHeight;
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH, 40) delegate:self titleNames:self.titleArray configure:configure];
    [self.view addSubview:self.pageTitleView];
    
    DKAcademicController *vc1 = [[DKAcademicController alloc]init];
    [self.childVCArray addObject:vc1];

    DKReCommendController *vc2 = [[DKReCommendController alloc]init];
    [self.childVCArray addObject:vc2];

    DKHotArticleController *vc3 = [[DKHotArticleController alloc]init];
    [self.childVCArray addObject:vc3];
    
    DKHotArticleController *vc4 = [[DKHotArticleController alloc]init];
    [self.childVCArray addObject:vc4];
    
    DKRecruitController *vc5 = [[DKRecruitController alloc]init];
    [self.childVCArray addObject:vc5];
    
    
    CGFloat contentCollectionHeight = KSCREEN_HEIGHT - pageTitleY - KTabBarHeight - 40;
    self.pageContentCollectionView = [[SGPageContentCollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.pageTitleView.frame), KSCREEN_WIDTH, contentCollectionHeight) parentVC:self childVCs:self.childVCArray];
    self.pageContentCollectionView.backgroundColor = [UIColor greenColor];

    self.pageContentCollectionView.delegatePageContentCollectionView = self;
    [self.view addSubview:self.pageContentCollectionView];
}


-(void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex{
   
    [self.pageContentCollectionView setPageContentCollectionViewCurrentIndex:selectedIndex];
    
}


-(void)pageContentCollectionView:(SGPageContentCollectionView *)pageContentCollectionView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex{
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}


@end
