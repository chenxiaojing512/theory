//
//  DKCommendController.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//推荐

#import "DKReCommendController.h"
#import "DKScholarCell.h"//学者
#import "DKRecommentHeaderV.h"//头部
#import "DKRecommendFooterV.h"//底部
#import "DKLiteratureCell.h"//推荐文献
@interface DKReCommendController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tView;

@end

@implementation DKReCommendController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

-(void)setupTableView{
    
    self.tView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tView.delegate = self;
    self.tView.dataSource = self;
    [self.tView kjRegisXibName:@"DKScholarCell"];
    [self.tView kjRegisXibName:@"DKLiteratureCell"];
    self.tView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tView];
    [self.tView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.edges.equalTo(self.view);
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 36.f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    DKRecommentHeaderV *hv = [[DKRecommentHeaderV alloc]init];
    return  hv;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 0) {
        return 44.f;
    }
    return 0.f;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    
    DKRecommendFooterV *fv = [[DKRecommendFooterV alloc]init];
    return fv;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    if (indexPath.section == 0) {
        return 102.f;
    }
    return 200.f;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        DKScholarCell *cell = [tableView kjDequeueCell:[DKScholarCell class] index:indexPath];
          return cell;
    }else{
        DKLiteratureCell *cell = [tableView kjDequeueCell:[DKLiteratureCell class] index:indexPath];
          return cell;
    }
  
}


@end

