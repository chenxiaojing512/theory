//
//  DKSearchResultController.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKSearchResultController.h"
#import "DKAcademicController.h"
#import "DKReCommendController.h"
@interface DKSearchResultController ()<SGPageTitleViewDelegate,SGPageContentCollectionViewDelegate>

@property (nonatomic,strong)NSMutableArray *titleArray;
@property (nonatomic,strong)NSMutableArray *childVCArray;
@property (nonatomic,strong) SGPageTitleView *pageTitleView;
@property (nonatomic,strong)SGPageContentCollectionView *pageContentCollectionView;


@end

@implementation DKSearchResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
    self.titleArray = [[NSMutableArray alloc]init];
    self.childVCArray = [[NSMutableArray alloc]init];
    [self setPageView];
}

-(void)setPageView{
    
    NSArray *titleArr = @[@{@"sid":@"0",@"name":@"学术圈"},
                             @{@"sid":@"1",@"name":@"推荐"},
//                             @{@"sid":@"2",@"name":@"热门论文"},
//                             @{@"sid":@"3",@"name":@"视频"},
//                             @{@"sid":@"3",@"name":@"招聘"},
    ];
    
    for (NSInteger i = 0;i<titleArr.count ; i++) {
       [self.titleArray addObject:titleArr[i][@"name"]];
    }
    
    SGPageTitleViewConfigure *configure = [[SGPageTitleViewConfigure alloc]init];
    configure.showIndicator = YES;
    configure.indicatorColor = UIColorFromRGB(0x1E49AF);
    configure.indicatorHeight = 2;
    configure.titleColor = UIColorFromRGB(0x5F7192);
    configure.titleSelectedColor = UIColorFromRGB(0x000000);
    configure.titleFont = [UIFont systemFontOfSize:13];
    configure.titleSelectedFont = [UIFont systemFontOfSize:18];
//    configure.titleAdditionalWidth = 10;
    configure.indicatorAdditionalWidth = -10;
    
    CGFloat pageTitleY = KStatusBarHeight + KNavBarHeight;
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0,pageTitleY , KSCREEN_WIDTH, 40) delegate:self titleNames:self.titleArray configure:configure];
    [self.view addSubview:self.pageTitleView];
    
    
    DKAcademicController *vc1 = [[DKAcademicController alloc]init];
    [self.childVCArray addObject:vc1];

    DKReCommendController *vc2 = [[DKReCommendController alloc]init];
    [self.childVCArray addObject:vc2];

     
        
        
    CGFloat contentCollectionHeight = KSCREEN_HEIGHT - pageTitleY - KTabBarHeight;
    self.pageContentCollectionView = [[SGPageContentCollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.pageTitleView.frame), KSCREEN_WIDTH, contentCollectionHeight) parentVC:self childVCs:self.childVCArray];
    self.pageContentCollectionView.backgroundColor = [UIColor greenColor];

    self.pageContentCollectionView.delegatePageContentCollectionView = self;
    [self.view addSubview:self.pageContentCollectionView];
}


-(void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex{
   
    [self.pageContentCollectionView setPageContentCollectionViewCurrentIndex:selectedIndex];
    
}


-(void)pageContentCollectionView:(SGPageContentCollectionView *)pageContentCollectionView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex{
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}





@end
