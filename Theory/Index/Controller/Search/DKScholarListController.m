//
//  DKScholarListController.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKScholarListController.h"
#import "DKScholarCell.h"
@interface DKScholarListController ()

@end

@implementation DKScholarListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView kjRegisXibName:@"DKScholarCell"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 42.5;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *fv = [UIView new];
    UILabel *lb_title = [[UILabel alloc]init];
    lb_title.text = @"共有7位同名学者 ";
    lb_title.font = [UIFont systemFontOfSize:16];
    lb_title.textColor = UIColorFromRGB(0x888888);
    [fv addSubview:lb_title];
    [lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fv).offset(14);
        make.centerY.equalTo(fv);
    }];
    return fv;
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DKScholarCell *cell = [tableView kjDequeueCell:[DKScholarCell class] index:indexPath];
    return cell;
}


@end
