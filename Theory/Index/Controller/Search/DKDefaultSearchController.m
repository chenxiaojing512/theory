//
//  DKDefaultSearchController.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//搜索默认

#import "DKDefaultSearchController.h"
#import "DKSearchResultController.h"
#import "DKSearchHeaderV.h"//头部
#import "DKSearchHotWordCell.h"//热门词
#import "DKSearchHotScholarCell.h"//热门学者
#define kCollectionWidth (kScreenWidth - 41 - 14)
#define kMarginItem 21
#define kItemWidth (kCollectionWidth - kMarginItem)/2

@interface DKDefaultSearchController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchResultsUpdating,UISearchControllerDelegate>

@property (nonatomic,strong)UISearchController *searchController;
@property (nonatomic,strong)UICollectionView *cView;
@property (nonatomic,strong)DKSearchResultController *resultVC;
@end

@implementation DKDefaultSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

-(void)setupUI{
    
    self.resultVC = [[DKSearchResultController alloc]init];
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:self.resultVC];
      // 设置结果更新代理
    self.searchController.delegate = self;
    self.searchController.searchResultsUpdater = self;

      // 因为在当前控制器展示结果, 所以不需要这个透明视图
//    search.dimsBackgroundDuringPresentation = NO;
    self.navigationController.extendedLayoutIncludesOpaqueBars = YES;
    self.searchController.searchBar.placeholder = @"搜索文献标题、作者、关键词、摘要等";
    self.searchController.searchBar.qmui_placeholderColor = UIColorFromRGB(0xC2C6CE);
    self.searchController.searchBar.qmui_font = [UIFont systemFontOfSize:14];
    self.searchController.searchBar.barTintColor = [UIColor redColor];
    UIImage *image = [UIImage qmui_imageWithColor:UIColorFromRGB(0xF9F9F9) size:CGSizeMake(40.0, 32.0) cornerRadius:16];
    image = [image qmui_imageWithBorderColor:UIColorFromRGB(0xE6E6E6) borderWidth:0.5 cornerRadius:16];

    [self.searchController.searchBar setSearchFieldBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(16, 16, 16, 16)] forState:UIControlStateNormal];

    if (@available(iOS 13.0, *)) {
        
        self.searchController.searchBar.searchTextField.leftViewMode = UITextFieldViewModeNever;
        self.searchController.searchBar.searchTextField.leftView = nil;
        UIImageView *search = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        search.image = [UIImage imageNamed:@"search"];
        self.searchController.searchBar.searchTextField.rightView = search;
        self.searchController.searchBar.searchTextField.rightViewMode =  UITextFieldViewModeAlways;
        
    } else {
        
        
    }
    
    self.navigationItem.titleView = self.searchController.searchBar;
            
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 21;
    self.cView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.cView.backgroundColor = [UIColor whiteColor];
    self.cView.delegate = self;
    self.cView.dataSource = self;
    [self.cView kjRegisCell:[DKSearchHotWordCell class]];
    [self.cView kjRegisXibName:@"DKSearchHotScholarCell"];
    [self.cView registerClass:[DKSearchHeaderV class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DKSearchHeaderVID"];
    [self.view addSubview:self.cView];
    [self.cView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(41);
        make.right.equalTo(self.view).offset(-14);
        make.top.bottom.equalTo(self.view);
    }];
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 8;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{

    return CGSizeMake(kCollectionWidth, 46);
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    if (kind == UICollectionElementKindSectionHeader) {

        DKSearchHeaderV *hv = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DKSearchHeaderVID" forIndexPath:indexPath];
        hv.lb_title.text = @"热门学者";
        return hv;
    }
    return nil;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        return CGSizeMake(kItemWidth, kItemWidth*32/150);

    }else{
        
       return CGSizeMake(kItemWidth, 54);

    }
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        DKSearchHotWordCell *cell = [collectionView kjDequeueCell:[DKSearchHotWordCell class] index:indexPath];
        return cell;
        
    }else{
        
        DKSearchHotScholarCell *cell = [collectionView kjDequeueCell:[DKSearchHotScholarCell class] index:indexPath];
        return cell;
    }
}

//点击cell
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        
    }else{
        
    }
}


-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    if (self.resultVC) {
//           self.resultVC.kjKeyword = searchController.searchBar.text;
      }
    
}

@end
