//
//  DKAcademicController.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//学术圈

#import "DKAcademicController.h"
#import "DKInfoView.h"
#import "DKCommentFooterV.h"
#import "DKAcademicShareCell.h"
#import "DKAcademicPhotoCell.h"
@interface DKAcademicController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tView;

@end

@implementation DKAcademicController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

-(void)setupTableView{
    
    self.tView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tView.delegate = self;
    self.tView.dataSource = self;
    [self.tView kjRegisXibName:@"DKAcademicShareCell"];
    [self.tView kjRegisXibName:@"DKAcademicPhotoCell"];
    self.tView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tView];
    [self.tView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.edges.equalTo(self.view);
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10 + 35 + 20;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    DKInfoView *hv = [DKInfoView initView];
    //关注点击事件  pid 作者id
    [hv setAttentionBlock:^(NSString * _Nonnull pid) {
        
    }];
    return  hv;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 44;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    DKCommentFooterV *fv = [[DKCommentFooterV alloc]init];
    return fv;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 200;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1) {
        DKAcademicPhotoCell *cell = [tableView kjDequeueCell:[DKAcademicPhotoCell class] index:indexPath];
          return cell;
    }else{
        DKAcademicShareCell *cell = [tableView kjDequeueCell:[DKAcademicShareCell class] index:indexPath];
          return cell;
    }
}



@end
