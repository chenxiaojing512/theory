//
//  DKHotArticleController.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//热门论文

#import "DKHotArticleController.h"
#import "DKHotArticleFirstCell.h"
#import "DKArticleNormalCell.h"

@interface DKHotArticleController ()<GroupShadowTableViewDelegate,GroupShadowTableViewDataSource>
@property (nonatomic,strong)GroupShadowTableView *tView;
@end

@implementation DKHotArticleController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

-(void)setupTableView{
    self.tView = [[GroupShadowTableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tView.groupShadowDelegate = self;
    self.tView.groupShadowDataSource = self;
    self.tView.showSeparator = NO;
    [self.view addSubview:self.tView];
    [self.tView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

//MARK: - GroupShadowTableViewDataSource
- (NSInteger)numberOfSectionsInGroupShadowTableView:(GroupShadowTableView *)tableView {
    return 3;
}

- (NSInteger)groupShadowTableView:(GroupShadowTableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)groupShadowTableView:(GroupShadowTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == 0) {
        
        static NSString *cellID = @"cellID";
          
          DKHotArticleFirstCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
          if (cell == nil) {
              cell = [[DKHotArticleFirstCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
          }
          
          return cell;
        
    }else{
        
        static NSString *cellID = @"cellID";
          
          DKArticleNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
          if (cell == nil) {
              cell = [[DKArticleNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
          }
          
          return cell;
    }
  
    
}


//MARK: - GroupShadowTableViewDelegate
- (CGFloat)groupShadowTableView:(GroupShadowTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 175;
    }
    return 90;
}

- (void)groupShadowTableView:(GroupShadowTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

@end
