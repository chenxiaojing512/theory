//
//  DKInfoView.h
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKInfoView : UIView

+(instancetype)initView;

//关注事件
@property (nonatomic,copy)void (^attentionBlock)(NSString *pid);

@end

NS_ASSUME_NONNULL_END
