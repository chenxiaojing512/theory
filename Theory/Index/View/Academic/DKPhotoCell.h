//
//  DKPhotoCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKPhotoCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *img_pic;

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIButton *btn_more;


@end

NS_ASSUME_NONNULL_END
