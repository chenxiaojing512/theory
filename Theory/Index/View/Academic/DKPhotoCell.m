//
//  DKPhotoCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKPhotoCell.h"

@implementation DKPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backView.backgroundColor =  [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
}

@end
