//
//  DKAcademicPhotoCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKAcademicPhotoCell.h"
#import "DKPhotoCell.h"

@implementation DKAcademicPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.cView kjRegisXibName:@"DKPhotoCell"];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 9;
    layout.minimumInteritemSpacing = 0;
    layout.itemSize = CGSizeMake(70, 70);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.cView.collectionViewLayout = layout;
    self.cView.delegate = self;
    self.cView.dataSource = self;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 5;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DKPhotoCell *cell = [collectionView kjDequeueCell:[DKPhotoCell class] index:indexPath];
    return cell;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
