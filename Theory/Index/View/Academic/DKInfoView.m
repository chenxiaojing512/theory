//
//  DKInfoView.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKInfoView.h"

@implementation DKInfoView

+(instancetype)initView{
    
    return [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass(self) owner:nil options:nil]lastObject];
}



//关注事件
- (IBAction)attentionClick:(UIButton *)sender {
    
    if (self.attentionBlock) {
        self.attentionBlock(@"");
    }
}


@end
