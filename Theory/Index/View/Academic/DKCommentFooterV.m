//
//  DKCommentFooterV.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//学术圈  评论点赞view


#import "DKCommentFooterV.h"

@implementation DKCommentFooterV

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        UIView *topLine  = [[UIView alloc]init];
        topLine.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [self addSubview:topLine];
        [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        
        UIView *middleView = [[UIView alloc]init];
        middleView.backgroundColor = [UIColor whiteColor];
        [self addSubview:middleView];
        
        [middleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topLine.mas_bottom);
            make.left.right.equalTo(self);
            make.height.mas_equalTo(39);
        }];
        
        
        UIView *verticalLine = [[UIView alloc]init];
        verticalLine.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [middleView addSubview:verticalLine];
        
        [verticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(0.5);
            make.top.equalTo(middleView).offset(7.5);
            make.bottom.equalTo(middleView).offset(-7.5);
            make.center.equalTo(middleView);
        }];
        
        
        UIButton *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [commentBtn setImage:[UIImage imageNamed:@"comment_normal"] forState:UIControlStateNormal];
        [commentBtn setTitle:@" 评论 44" forState:UIControlStateNormal];
        commentBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [commentBtn setTitleColor:UIColorFromRGB(0x5F7192) forState:UIControlStateNormal];
        [middleView addSubview:commentBtn];
        
        [commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(middleView);
            make.right.equalTo(verticalLine);
        }];
        
        
        UIButton *zanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [zanBtn setImage:[UIImage imageNamed:@"zan_normal"] forState:UIControlStateNormal];
       [zanBtn setTitle:@" 点赞 44" forState:UIControlStateNormal];
       zanBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [zanBtn setTitleColor:UIColorFromRGB(0x5F7192) forState:UIControlStateNormal];

       [middleView addSubview:zanBtn];
       
       [zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.bottom.right.equalTo(middleView);
           make.left.equalTo(verticalLine);
       }];
        
        UIView *spaceV = [[UIView alloc]init];
        spaceV.backgroundColor = UIColorFromRGB(0xF7F7F7);
        [self addSubview:spaceV];
        
        [spaceV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(middleView.mas_bottom);
            make.height.mas_equalTo(5);
        }];
    }
    return self;
}
@end
