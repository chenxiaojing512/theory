//
//  DKRecommendFooterV.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKRecommendFooterV.h"

@implementation DKRecommendFooterV

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        UIButton *btn_change = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn_change setTitle:@"换一换" forState:UIControlStateNormal];
        [btn_change setTitleColor:UIColorFromRGB(0x5F7192) forState:UIControlStateNormal];
        [btn_change setImage:[UIImage imageNamed:@"index_change"] forState:UIControlStateNormal];
        btn_change.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn_change addTarget:self action:@selector(changeClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn_change];
        
        [btn_change mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    
    return self;
}


//换一换
-(void)changeClick:(UIButton*)sender{
    Plog(@"换一换");
    
}

@end
