//
//  DKRecommentHeaderV.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKRecommentHeaderV.h"

@implementation DKRecommentHeaderV

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.img_pic = [[UIImageView alloc]init];
        self.img_pic.image = [UIImage imageNamed:@"index_follow"];
        [self addSubview:self.img_pic];
        
        self.lb_title = [[UILabel alloc]init];
        self.lb_title.text = @"可能感兴趣的人";
        self.lb_title.font = [UIFont systemFontOfSize:12];
        self.lb_title.textColor = UIColorFromRGB(0x1E49AF);
        [self addSubview:self.lb_title];
        
        [self.img_pic mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.centerY.equalTo(self);
            make.width.height.mas_equalTo(16);
        }];
        
        [self.lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.img_pic.mas_right).offset(5);
            make.centerY.equalTo(self);
        }];
    }
    
    return self;
}

@end
