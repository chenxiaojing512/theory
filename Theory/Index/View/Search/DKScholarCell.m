//
//  DKScholarCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKScholarCell.h"

@implementation DKScholarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lb_num.layer.masksToBounds = YES;
    self.lb_num.layer.cornerRadius = 12;
    
    
    [self.btn_follow gradientButtonWithSize:CGSizeMake(60, 24) colorArray:@[UIColorFromRGB(0x84B8FF),UIColorFromRGB(0x1E49AF)] percentageArray:@[@(0.2),@(1)] gradientType:GradientFromLeftToRight];

}

#pragma mark ----关注事件
- (IBAction)attentionClick:(UIButton *)sender {
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
