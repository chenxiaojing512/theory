//
//  DKSearchHeaderV.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKSearchHeaderV.h"

@implementation DKSearchHeaderV

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupUI];
    }
    
    return self;
}

-(void)setupUI{
    
    self.logo = [[UIImageView alloc]init];
    self.logo.image = [UIImage imageNamed:@"hot"];
    [self addSubview:self.logo];
    
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(16);
    }];
    
    self.lb_title = [[UILabel alloc]init];
    self.lb_title.text = @"热门学者";
    self.lb_title.font = [UIFont systemFontOfSize:12];
    self.lb_title.textColor = UIColorFromRGB(0xFF893B);
    [self addSubview:self.lb_title];
    
    [self.lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logo.mas_right).offset(3);
        make.centerY.equalTo(self);
    }];
}
@end
