//
//  DKScholarCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKScholarCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img_pic;

@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_school;

@property (weak, nonatomic) IBOutlet UILabel *lb_num;

//关注
@property (weak, nonatomic) IBOutlet UIButton *btn_follow;


@end

NS_ASSUME_NONNULL_END
