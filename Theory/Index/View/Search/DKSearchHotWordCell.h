//
//  DKSearchHotWordCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKSearchHotWordCell : UICollectionViewCell

@property (nonatomic,strong)UILabel *lb_title;

@end

NS_ASSUME_NONNULL_END
