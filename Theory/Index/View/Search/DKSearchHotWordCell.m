//
//  DKSearchHotWordCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/19.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKSearchHotWordCell.h"

@implementation DKSearchHotWordCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {        
        self.lb_title = [[UILabel alloc]init];
        self.lb_title.text = @"资产证券化";
        self.lb_title.textColor = UIColorFromRGB(0x888888);
        self.lb_title.font = [UIFont systemFontOfSize:14];
        self.lb_title.layer.masksToBounds = YES;
        self.lb_title.layer.cornerRadius = 12;
        self.lb_title.layer.borderColor = UIColorFromRGB(0xE6E6E6).CGColor;
        self.lb_title.layer.borderWidth = 0.5;
        self.lb_title.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.lb_title];

        [self.lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
    }
    
    return self;
}
@end
