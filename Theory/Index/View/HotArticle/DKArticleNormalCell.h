//
//  DKArticleNormalCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKArticleNormalCell : UITableViewCell

@property (nonatomic,strong)UILabel *lb_title;

@property (nonatomic,strong)UILabel *lb_name;

@end

NS_ASSUME_NONNULL_END
