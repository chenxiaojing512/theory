//
//  DKHotArticleCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//弃用

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKHotArticleCell : UITableViewCell <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lb_title;

@property (weak, nonatomic) IBOutlet UILabel *lb_intro;

@property (weak, nonatomic) IBOutlet UILabel *lb_name;


@end

NS_ASSUME_NONNULL_END
