//
//  DKHotArticleFirstCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/18.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKHotArticleFirstCell : UITableViewCell

@property (nonatomic,strong)UIImageView *backImg;

@property (nonatomic,strong)UILabel *lb_title;

@property (nonatomic,strong)UILabel *lb_intro;

@property (nonatomic,strong)UILabel *lb_name;

@property (nonatomic,strong)UIButton *btn_more;


@end

NS_ASSUME_NONNULL_END
