//
//  DKHotArticleFirstCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/18.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKHotArticleFirstCell.h"

@implementation DKHotArticleFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

-(void)setupUI{
    
    UIImageView *bgImg = [[UIImageView alloc]init];
    bgImg.backgroundColor = [UIColor blueColor];
    bgImg.layer.masksToBounds = YES;
    bgImg.layer.cornerRadius = 10;
    self.backImg = bgImg;
    [self addSubview:self.backImg];
    
    [self.backImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    self.lb_title = [[UILabel alloc]init];
    self.lb_title.text = @"中国P2P网络借贷平台信用认证机制研究——来自“人人贷”的经验证据";
    self.lb_title.font = [UIFont systemFontOfSize:15];
    self.lb_title.textColor = UIColorFromRGB(0xFFFFFF);
    self.lb_title.numberOfLines = 2;
    [self addSubview:self.lb_title];
    
    [self.lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        
    }];
    
    
    UIView *lineV = [[UIView alloc]init];
    lineV.backgroundColor = UIColorFromRGB(0xAFAFAF);
    [self addSubview:lineV];
    
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self.lb_title.mas_bottom).offset(10);
        make.width.mas_equalTo(235);
        make.height.mas_equalTo(0.5);
    }];
    
    
    self.lb_intro = [[UILabel alloc]init];
    self.lb_intro.text = @"摘要：2008年国际金融危机爆发后，金融风险的内生积聚成为货币理论和2行…";
    self.lb_intro.font = [UIFont systemFontOfSize:13];
    self.lb_intro.textColor = UIColorFromRGB(0xBFBFBF);
    self.lb_intro.numberOfLines = 2;
    [self addSubview:self.lb_intro];
    
    [self.lb_intro mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(lineV.mas_bottom).offset(10);
        make.width.equalTo(lineV);
    }];

    
    self.lb_name = [[UILabel alloc]init];
    self.lb_name.text = @"王会娟，廖理 - 2014";
    self.lb_name.font = [UIFont systemFontOfSize:11];
    self.lb_name.textColor = UIColorFromRGB(0xBFBFBF);
    [self addSubview:self.lb_name];
       
    [self.lb_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self.lb_intro.mas_bottom).offset(5);
        make.width.equalTo(lineV);
    }];
    
    self.btn_more = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn_more setTitle:@"更多相关论文" forState:UIControlStateNormal];
    [self.btn_more setTitleColor:UIColorFromRGB(0x1E49AF) forState:UIControlStateNormal];
    self.btn_more.layer.masksToBounds = YES;
    self.btn_more.layer.cornerRadius = 10;
    self.btn_more.titleLabel.font = [UIFont systemFontOfSize:12];
    self.btn_more.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.btn_more];
    
    [self.btn_more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.bottom.equalTo(self).offset(-10);
        make.width.mas_equalTo(92);
        make.height.mas_equalTo(24);
    }];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
