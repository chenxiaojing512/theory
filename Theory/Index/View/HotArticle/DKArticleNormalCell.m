//
//  DKArticleNormalCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKArticleNormalCell.h"

@implementation DKArticleNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

-(void)setupUI{
    
    self.lb_title = [[UILabel alloc]init];
    self.lb_title.font = [UIFont systemFontOfSize:15];
    self.lb_title.textColor = UIColorFromRGB(0x000000);
    self.lb_title.text = @"1222222222";
    self.lb_title.numberOfLines = 2;
    [self addSubview:self.lb_title];
    
    [self.lb_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.top.equalTo(self).offset(15);
    }];
    
    self.lb_name = [[UILabel alloc]init];
    self.lb_name.font = [UIFont systemFontOfSize:12];
    self.lb_name.textColor = UIColorFromRGB(0x9A9CA7);
    self.lb_name.text = @"1222222222";
    [self addSubview:self.lb_name];
    
    [self.lb_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.top.equalTo(self.lb_title.mas_bottom).offset(5);
    }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
