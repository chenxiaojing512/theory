//
//  DKRecruitCell.h
//  Theory
//
//  Created by xiaojing on 2020/6/24.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKRecruitCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *img_pic;

@property (weak, nonatomic) IBOutlet UILabel *lb_position;

@property (weak, nonatomic) IBOutlet UILabel *lb_company;

@property (weak, nonatomic) IBOutlet UILabel *lb_money;

@end

NS_ASSUME_NONNULL_END
