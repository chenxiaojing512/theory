//
//  DKRecruitCell.m
//  Theory
//
//  Created by xiaojing on 2020/6/24.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKRecruitCell.h"

@implementation DKRecruitCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.masksToBounds = YES;
    self.bgView.layer.cornerRadius = 10;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
