//
//  DKNavView.m
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "DKSearchView.h"
@implementation DKSearchView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupUI];
    }
    
    return self;
}



-(void)setupUI{
    
    UIImageView *logo = [[UIImageView alloc]init];
    logo.image = [UIImage imageNamed:@"logo"];
    [self addSubview:logo];

    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(49);
        make.height.mas_equalTo(24);
    }];
    
    UIView *searchV = [[UIView alloc]init];
    searchV.backgroundColor = UIColorFromRGB(0xF9F9F9);
    searchV.layer.borderColor = UIColorFromRGB(0xE6E6E6).CGColor;
    searchV.layer.borderWidth = 0.5;
    searchV.layer.masksToBounds = YES;
    searchV.layer.cornerRadius = 15;
    searchV.userInteractionEnabled = YES;
    //跳转搜索默认页
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(searchTap:)];
    [searchV addGestureRecognizer:tap];
    
    [self addSubview:searchV];
    
    [searchV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(logo.mas_right).offset(9);
//        make.top.equalTo(self).offset(5);
        make.right.equalTo(self).offset(0);
        make.height.mas_equalTo(32);
        make.centerY.equalTo(self);
    }];
    
    UIImageView *searchIcon = [[UIImageView alloc]init];
    searchIcon.image = [UIImage imageNamed:@"search"];
    [searchV addSubview:searchIcon];
    [searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(14.8);
        make.centerY.equalTo(searchV);
        make.right.equalTo(searchV).offset(-12);
    }];
    
    
    UILabel *lb_search = [[UILabel alloc]init];
    lb_search.text = @"搜索文献标题、作者、关键词、摘要等";
    lb_search.textColor = UIColorFromRGB(0xC2C6CE);
    lb_search.font = [UIFont systemFontOfSize:14];
    [self addSubview:lb_search];

    [lb_search mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchV).offset(9);
        make.centerY.equalTo(searchV);
             
    }];
}


-(void)searchTap:(UITapGestureRecognizer *)tap{
    
    if (self.searchTapBlock) {
        self.searchTapBlock();
    }
}


@end
