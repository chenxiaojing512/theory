//
//  MainTabBarController.m
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import "MainTabBarController.h"
#import "DKIndexController.h"//首页
#import "DKLiveController.h"//直播
#import "DKMeetingController.h"//会议
#import "DKProfileController.h"//我的

@interface MainTabBarController ()

@end

@implementation MainTabBarController


+ (void)initialize
{
    UITabBarItem *appearance = [UITabBarItem appearance];
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    [appearance setTitleTextAttributes:attrs forState:UIControlStateSelected];
    [appearance setTitleTextAttributes:attrs forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    [self setupChildController];
}


-(void)setupChildController{
    
    DKIndexController *indexVC = [[DKIndexController alloc]init];
    indexVC.hidesBottomBarWhenPushed = NO;
    [self setupOneChildViewController:indexVC title:@"主页" image:@"index_normal" selectedImage:@"index_select"];
    
    DKLiveController *liveVC = [[DKLiveController alloc]init];
    liveVC.hidesBottomBarWhenPushed = NO;
    [self setupOneChildViewController:liveVC title:@"直播" image:@"live_normal" selectedImage:@"live_select"];
    
    DKMeetingController *meetingVC = [[DKMeetingController alloc]init];
    meetingVC.hidesBottomBarWhenPushed = NO;
    [self setupOneChildViewController:meetingVC title:@"会议" image:@"meeting_normal" selectedImage:@"meeting_select"];
    
    DKProfileController *myVC = [[DKProfileController alloc]init];
    myVC.hidesBottomBarWhenPushed = NO;
    [self setupOneChildViewController:myVC title:@"我的" image:@"profile_normal" selectedImage:@"profile_select"];
    
}

- (void)setupOneChildViewController:(QMUICommonViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage{
    
    UINavigationController *navc =  [[UINavigationController alloc]initWithRootViewController:vc];
    
    navc.tabBarItem.title = title;
    navc.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    navc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    if (@available(iOS 13, *)){
        
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:30/256.0 green:73/256.0 blue:175/256.0 alpha:1]];
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithRed:30/256.0 green:73/256.0 blue:175/256.0 alpha:0.5]];
        
    }else
    {
       
        [navc.tabBarItem setTitleTextAttributes:@{
            NSForegroundColorAttributeName:[UIColor colorWithRed:30/256.0 green:73/256.0 blue:175/256.0 alpha:0.5]
                                       }
                            forState:UIControlStateNormal];
        [navc.tabBarItem setTitleTextAttributes:@{
                                       NSForegroundColorAttributeName:[UIColor colorWithRed:30/256.0 green:73/256.0 blue:175/256.0 alpha:1]
                                       }
                            forState:UIControlStateSelected];
    }
   

    

 
 
    
    [self addChildViewController:navc];
    
}
@end
