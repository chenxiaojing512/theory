//
//  DKBaseTableController.m
//  Loan
//
//  Created by 陈晓晶 on 2019/5/6.
//  Copyright © 2019 陈晓晶. All rights reserved.
//

#import "DKBaseTableController.h"

@interface DKBaseTableController ()

@end

@implementation DKBaseTableController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
    // 导航栏title的颜色
    self.titleView.titleLabel.textColor = UIColorFromRGB(0x000000);
    // 导航栏title的字体
    self.titleView.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightMedium];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0x515151)];

    self.view.backgroundColor = [UIColor whiteColor];
    if (self.navigationController != nil &&
        self.navigationController.viewControllers.firstObject != self) {
        // push进来的话，会自动创建返回按钮
        [self setNaviBackBarButtomItem];
    }
}

///创建返回按钮 默认push时会自动创建，present根据需要自己调用创建
- (void)setNaviBackBarButtomItem {
    //替换系统自带的backItem
    QMUINavigationButton *back = [[QMUINavigationButton alloc] initWithType:QMUINavigationButtonTypeImage];
    UIImage *image = [[self kjNavigationBackItemImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [back setImage:image forState:UIControlStateNormal];
    back.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 10);
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem qmui_itemWithButton:back
                                                                          target:self
                                                                          action:@selector(didNavigationBackItemAction)];
}

-(void)setNaviRightBarButtomItem:(NSString*)title titleColor:(UIColor*)titleColor{
    
    QMUINavigationButton *registerBtn = [[QMUINavigationButton alloc]initWithType:QMUINavigationButtonTypeNormal title:title];
    [registerBtn setTitleColor:titleColor forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem =  [UIBarButtonItem qmui_itemWithButton:registerBtn target:self action:@selector(NaviRightBarButtomClick:)];
}

//设置导航栏右边的按钮图片格式
-(void)setNaviRightBarButtomImageItem:(NSString*)imageStr{
    
    QMUINavigationButton *registerBtn = [[QMUINavigationButton alloc]initWithType:QMUINavigationButtonTypeNormal title:nil];
    [registerBtn setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem =  [UIBarButtonItem qmui_itemWithButton:registerBtn target:self action:@selector(NaviRightBarButtomClick:)];
}


//设置导航栏左边的按钮图片格式
-(void)setNaviLeftBarButtomImageItem:(NSString*)imageStr{
    
    QMUINavigationButton *registerBtn = [[QMUINavigationButton alloc]initWithType:QMUINavigationButtonTypeNormal title:nil];
    [registerBtn setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem =  [UIBarButtonItem qmui_itemWithButton:registerBtn target:self action:@selector(NaviLeftBarButtomClick:)];
}

-(void)NaviLeftBarButtomClick:(UIButton*)sender{
    
}

- (BOOL)forceEnableInteractivePopGestureRecognizer {
    return YES;
}

-(void)NaviRightBarButtomClick:(UIButton*)sender{
    
    NSLog(@"父类注册");
}

//MARK: - 下面方法子类可重写

/// 返回按钮点击事件
- (void)didNavigationBackItemAction {
    if (self.navigationController.viewControllers.firstObject == self) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/// 返回按钮的图片
- (UIImage *)kjNavigationBackItemImage {
    return [UIImage imageNamed:@"icon_nav_back"];
}
- (UIColor *)titleViewTintColor {
    return UIColorFromRGB(0x000000);
}

/// 设置导航栏的背景图，默认为 NavBarBackgroundImage
- (UIImage *)navigationBarBackgroundImage {
    return [UIImage imageWithColor:[UIColor whiteColor]];
}

/// 设置导航栏底部的分隔线图片，默认为 NavBarShadowImage，必须在 navigationBar 设置了背景图后才有效（系统限制如此）
- (UIImage *)navigationBarShadowImage {
    return [UIImage imageWithColor:[UIColor whiteColor]];
}

/// 设置系统返回按钮title，如果返回nil则使用系统默认的返回按钮标题
- (nullable NSString *)backBarButtonItemTitleWithPreviousViewController:(nullable UIViewController *)viewController {
    return @"";
}

/**
 *  当切换界面时，如果不同界面导航栏的显隐状态不同，可以通过 shouldCustomizeNavigationBarTransitionIfHideable 设置是否需要接管导航栏的显示和隐藏。从而不需要在各自的界面的 viewWillAppear 和 viewWillDisappear 里面去管理导航栏的状态。
 *  @see UINavigationController+NavigationBarTransition.h
 *  @see preferredNavigationBarHidden
 */
- (BOOL)shouldCustomizeNavigationBarTransitionIfHideable {
    return true;
}

/// 设置每个界面导航栏的显示/隐藏，为了减少对项目的侵入性，默认不开启这个接口的功能，只有当 shouldCustomizeNavigationBarTransitionIfHideable 返回 YES 时才会开启此功能。如果需要全局开启，那么就在 Controller 基类里面返回 YES；如果是老项目并不想全局使用此功能，那么则可以在单独的界面里面开启。
- (BOOL)preferredNavigationBarHidden {
    return false;
}

@end
