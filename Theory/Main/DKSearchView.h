//
//  DKNavView.h
//  Theory
//
//  Created by xiaojing on 2020/6/17.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKSearchView : UIView 

@property(nonatomic, assign) CGSize intrinsicContentSize;

@property (nonatomic,copy) void (^searchTapBlock)(void);

@end

NS_ASSUME_NONNULL_END
