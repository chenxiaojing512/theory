//
//  DKBaseTableController.h
//  Loan
//
//  Created by 陈晓晶 on 2019/5/6.
//  Copyright © 2019 陈晓晶. All rights reserved.
//

#import <QMUIKit/QMUIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DKBaseTableController : QMUICommonTableViewController
///创建返回按钮 默认push时会自动创建，present根据需要自己调用创建
- (void)setNaviBackBarButtomItem;

//设置导航栏右边的按钮
-(void)setNaviRightBarButtomItem:(NSString*)title titleColor:(UIColor*)titleColor;

//设置导航栏右边的按钮图片格式
-(void)setNaviRightBarButtomImageItem:(NSString*)imageStr;

//设置导航栏左边的按钮图片格式
-(void)setNaviLeftBarButtomImageItem:(NSString*)imageStr;

@end

NS_ASSUME_NONNULL_END
