//
//  AppDelegate.h
//  Theory
//
//  Created by xiaojing on 2020/6/16.
//  Copyright © 2020 陈晓晶. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic,strong) UIWindow *window;

@end

